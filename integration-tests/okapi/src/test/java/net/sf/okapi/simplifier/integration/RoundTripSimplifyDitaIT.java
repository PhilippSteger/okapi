package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;
import net.sf.okapi.roundtrip.integration.RoundTripDitaIT;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripSimplifyDitaIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_xmlstream-dita";
	private static final String DIR_NAME = "/dita/";
	private static final List<String> EXTENSIONS = Arrays.asList(".dita", ".ditamap");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".xliff";
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = XmlStreamFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripDitaIT.class);

	public RoundTripSimplifyDitaIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION, FILTER_CONSTRUCTOR);
	}

	@Ignore
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/dita/cdevstart98282.dita").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, null, null,
				new FileComparator.XmlComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void ditaFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.XmlComparator());
	}

	@Test
	public void ditaFilesSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
