package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripXliffIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_xliff";
	private static final String DIR_NAME = "/xliff/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xliff", ".xlf");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = XLIFFFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripXliffIT.class);

	public RoundTripXliffIT() {
		super(true, CONFIG_ID, DIR_NAME, EXTENSIONS, FILTER_CONSTRUCTOR);

		// probably failures to match inline codes or TextParts
		// another set of eyes should look at these in future
		addKnownFailingFile("non-segment-without-target.xlf");
		addKnownFailingFile("about_the.htm.xlf");
		addKnownFailingFile("keep_matching_bx_ex_ids.given.xlf");
		addKnownFailingFile("BetweenSegments.xlf");
		addKnownFailingFile("MultipleSegments.xlf");

		// these only fail with xliff merge - serialized passes
		addKnownFailingFile("test.txt.xlf");

		// need to debug
		addKnownFailingFile("DE_CALC_PHASE1.xlsx.sdlxliff");

		// issue with inline code annotations
		addKnownFailingFile("lqiTest.xlf");
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		setExtensions(EXTENSIONS);
		final File file =
				root.in("/xliff/lqiTest.xlf").asFile();
		runTest(new TestJob(CONFIG_ID, true, file, null, file.getParent(),
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void sdlXliff() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		setExtensions(Collections.singletonList(".sdlxliff"));
		realTestFiles("okf_xliff-sdl", true, new FileComparator.EventComparator());
	}

	@Test
	public void worldserverXliff() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		setExtensions(Collections.singletonList(".iwsxliff"));
		realTestFiles("okf_xliff-iws", true, new FileComparator.EventComparator());
	}

	@Test
	public void xliffFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		setExtensions(EXTENSIONS);
		realTestFiles(CONFIG_ID, true, new FileComparator.EventComparator());
	}

	@Test
	public void sdlXliffSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		setExtensions(Collections.singletonList(".sdlxliff"));
		realTestFiles("okf_xliff-sdl", true, new FileComparator.EventComparator());
	}

	@Test
	public void worldserverXliffSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		setExtensions(Collections.singletonList(".iwsxliff"));
		realTestFiles("okf_xliff-iws", true, new FileComparator.EventComparator());
	}

	@Test
	public void xliffSerialized() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		setExtensions(EXTENSIONS);
		realTestFiles(CONFIG_ID, true, new FileComparator.EventComparator());
	}
}
