package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.yaml.YamlFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@RunWith(JUnit4.class)
public class RoundTripYAMLMessageFormatIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_yaml";
	private static final String DIR_NAME = "/messageformat";
	private static final List<String> EXTENSIONS = Arrays.asList(".yml", ".yaml");
	private static final Supplier<IFilter> FILTER_CONSTRUCTOR = YamlFilter::new;
	final static FileLocation root = FileLocation.fromClass(RoundTripYAMLMessageFormatIT.class);

	public RoundTripYAMLMessageFormatIT() {
		super(true, CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.ARABIC, FILTER_CONSTRUCTOR);
	}


	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		final File file = root.in("/messageformat/YAML/expand/complex.yaml").asFile();
		runTest(new TestJob("okf_yaml@messageformat_expand", false, file, null, file.getParent(),
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void debug2() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		final File file = root.in("/messageformat/YAML/complex.yaml").asFile();
		runTest(new TestJob("okf_yaml@messageformat", false, file, null, file.getParent(),
				new FileComparator.EventComparator(), FILTER_CONSTRUCTOR));
	}

	@Test
	public void messageFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void messageSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
