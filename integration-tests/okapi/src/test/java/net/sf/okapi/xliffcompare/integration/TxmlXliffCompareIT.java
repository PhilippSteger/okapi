package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.txml.TXMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class TxmlXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_txml";
	private static final String DIR_NAME = "/txml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".txml");

	public TxmlXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, LocaleId.EMPTY, TXMLFilter::new);
	}

	@Test
	public void txmlXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(true, new FileComparator.XmlComparator());
	}
}
