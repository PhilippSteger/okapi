/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.common.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import java.util.LinkedList;
import java.util.List;

interface ResponsiveTableDialog {
    String[] open();

    final class Default implements ResponsiveTableDialog {
        private final Shell shell;
        private final String title;
        private final String[] inputLabels;
        private final String[] inputValues;
        private List<Text> inputs;
        private String[] outputValues;

        Default(final Shell shell, final String title, final String[] inputLabels, final String[] inputValues) {
            this.shell = shell;
            this.title = title;
            this.inputLabels = inputLabels;
            this.inputValues = inputValues;
        }

        @Override
        public String[] open() {
            this.shell.setText(this.title);
            this.shell.setLayout(new GridLayout());
            configureInputs();
            configureActions();
            configureGeneralListeners();
            this.shell.pack();
            this.shell.open();
            final Display display = this.shell.getShell().getDisplay();
            while (!this.shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
            return this.outputValues;
        }

        private void configureInputs() {
            this.inputs = new LinkedList<>();
            for (int i = 0; i < this.inputLabels.length; i++) {
                final Label l = new Label(this.shell, SWT.NONE);
                l.setText(this.inputLabels[i]);
                final Text t = new Text(this.shell, SWT.BORDER);
                final GridData tgd = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
                t.setLayoutData(tgd);
                this.inputs.add(t);
                if (this.inputValues.length == this.inputLabels.length) {
                    t.setText(this.inputValues[i]);
                }
            }
        }

        private void configureActions() {
            final SelectionAdapter actionsHandler = new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    if ("o".equals(e.widget.getData())) {
                        outputValues = inputs.stream()
                            .map(t -> t.getText())
                            .toArray(v -> new String[v]);
                    } else {
                        outputValues = new String[0];
                    }
                    shell.close();
                }
            };
            final OKCancelPanel actionsPanel = new OKCancelPanel(this.shell, SWT.NONE, actionsHandler, false);
            final GridData gd = new GridData(GridData.FILL_HORIZONTAL);
            actionsPanel.setLayoutData(gd);
            this.shell.setDefaultButton(actionsPanel.btOK);
        }

        private void configureGeneralListeners() {
            this.shell.addListener(SWT.Traverse, event -> {
                if (event.detail == SWT.TRAVERSE_ESCAPE) {
                    this.outputValues = new String[0];
                    this.shell.close();
                }
            });
        }
    }
}