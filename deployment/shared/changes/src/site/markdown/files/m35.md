# Changes from M34 to M35

<!-- MACRO{toc} -->

## Filters

* Markdown Filter

    * Fixed [issue #610](https://bitbucket.org/okapiframework/okapi/issues/610), adding two new options for processing. The
      "Translate Code Blocks" option (`translateCodeBlocks`),
      enabled by default, controls whether or not fenced code blocks
      should be extracted. The "Translate YAML Metadata Header" (`translateHeaderMetadata`),
      disabled by default, controls whether values from YAML-style "front
      matter" headers should be extracted.
    * Fixed [issue #651](https://bitbucket.org/okapiframework/okapi/issues/651), adding a new option for processing. The
      "Translate Image Alt Text" option (`translateImageAltText`),
      enabled by default, controls whether or not alt text from image
      references is extracted by default.
    * Improved default inline code finder regex used to process embedded
      HTML and XML tags.

* XLIFF Filter

    * Fixed the issue of the priority value being output in the
      annotates attribute for the `<note>` element.
    * Now, in SDLXLIFF files, the SDL-specific properties are attached to their respective segment (in read-only mode). The overall
      target TextContainer still holds the properties for the last segment, and if they are updated, the value in each segment is updated.
      This behavior is still not satisfactory, but at least allows readers to get the correct properties for each segment.

* OpenXML Filter

    * Added two new Excel options, "Translate Diagram Data" and
      "Translate Drawings". Both are disabled by default. "Translate
      Diagram Data" will extract text from SmartArt and other embedded
      diagrams, while "Translate Drawings" will extract text from embedded
      drawings such as text fields.
    * Added the "Use Included Slide Numbers Only" PowerPoint option.
      When checked, the configuration can specify specific slide numbers
      for extraction. Other slides will be ignored by the filter.
    * Fixed a bug in which `ctype` values for formatting codes
      extracted from PPTX documents showed up as `x-empty` rather
      than as useful style information.
    * Fixed a bug in which `ctype` values for formatting codes
      extracted from XLSX documents showed up as `x-empty` rather
      than as useful style information.
    * Fixed a bug when PPTX documents relate to external resources, e.g.
      hyperlinks that refer to videos.

* YAML Filter

    * Fix [issue #643](https://bitbucket.org/okapiframework/okapi/issues/643): Multi-line scalar string values will now be extracted
      as a single text unit, rather than one text unit per line. \
      **Warning: YAML files extracted with previous versions of Okapi may
      no longer merge successfully due to this change.**
    * Synchronize access to snake yaml parser to ensure thread-safety. This resolves [issue #658](https://bitbucket.org/okapiframework/okapi/issues/658).

* IDML Filter

    * Fixed [issue #636](https://bitbucket.org/okapiframework/okapi/issues/636), adding a new option "Skip discretionary hyphens" which is disabled by default.

* Archive Filter

    * Fixed [issue #641](https://bitbucket.org/okapiframework/okapi/issues/641), preventing some errors to be reported incorrectly.

* SDL Trados Package Filter

    * There is a new filter for SDL packages: .sdlppx and .sdlrpx files.

## Steps

* Translation Comparison

    * Improved the HTML report output (added link to go directly to the
      summary, right-aligned the values in the summary table).

* Quality Check

    * The black list file can now take an optional third column with a comment.
    * The black list file can now take an optional fourth column indicating the severity (0, 1 or 2).

* Microsoft Batch Submission Step

    * This step has been removed due to Microsoft disabling the
      underlying API. See additional notes under "Microsoft
      Translation Hub" in the Connectors section, below.

## Connectors

* Google MT

    * [Issue #635](https://bitbucket.org/okapiframework/okapi/issues/635): Added the "Use Phrase-Based MT" option. This option
      will force Google to use its phrase-based MT system, rather than
      neural MT, which it normally uses preferentially.

* MyMemory

    * Updated the API URL to use https instead of http.

* ModernMT

    * Fixed [issue #650](https://bitbucket.org/okapiframework/okapi/issues/650) where the connector was cast incorrectly to ITMQuery.
    * Fixed the issue of the optional context parameter not getting set.
    * Fixed [issue #655](https://bitbucket.org/okapiframework/okapi/issues/655) where the language parameters were not set.

* Microsoft Translation Hub

    * Removed `AddTranslation` and `AddTranslationList`
      methods. These methods were implemented using the
      `AddTranslation` and `AddTranslationArray` calls to
      the Microsoft Translator API; those API calls have been deprecated
      and will cease to function on January 31, 2018. Please see
      [the announcement from Microsoft](https://cognitive.uservoice.com/knowledgebase/articles/1828936-changes-to-the-collaborative-translation-functions)
      for more detail.

## Applications

* Rainbow

    * Fixed [issue #7](https://bitbucket.org/okapiframework/okapi/issues/7) in for Longhorn: BCONF files can support pipeline files with a lot more multi-bytes characters (e.g. Chinese).

* Tikal

    * Removed the `-a` option to add translations to a
      connector, as this was only supported for Microsoft.

## Connectors

* Pensieve TM

    * Fixed the filter writer so non-segmented entries with an empty target does not get imported.
      (It causes null pointer later if the source is retrieved).

## General

    * Fixed a bug that caused the `JSONEncoder` to not correctly
      JSON-escape nested content processed by a subfilter.
    * Updated the XLIFF-2 library to 1.1.7.
