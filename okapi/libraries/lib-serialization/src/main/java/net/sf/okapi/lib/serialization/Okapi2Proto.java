/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.lib.serialization;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.*;
import net.sf.okapi.common.query.MatchType;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.resource.TextFragment.TagType;

import java.util.*;

public class Okapi2Proto {
	static net.sf.okapi.proto.IResource toIResource(IResource value) {
		net.sf.okapi.proto.IResource.Builder builder = net.sf.okapi.proto.IResource.newBuilder();
		if (value.getId() != null) {
			builder.setId(value.getId());
		}

		for (String propName : value.getPropertyNames()) {
			builder.addProperties(toProperty(value.getProperty(propName)));
		}

		GenericAnnotations anns = value.getAnnotation(GenericAnnotations.class);
		if (anns != null) {
			builder.setGenericAnnotations(toGenericAnnotations(anns));
		}
		return builder.build();
	}

	private static net.sf.okapi.proto.GenericAnnotations toGenericAnnotations(GenericAnnotations anns) {
		net.sf.okapi.proto.GenericAnnotations.Builder builder = net.sf.okapi.proto.GenericAnnotations.newBuilder();
		builder.setEncodedAsString(anns.toString());
		for (GenericAnnotation ann : anns) {
			builder.addGenericAnnotations(toGenericAnnotation(ann));
		}
		return builder.build();
	}

	private static net.sf.okapi.proto.GenericAnnotation toGenericAnnotation(GenericAnnotation ann) {
		net.sf.okapi.proto.GenericAnnotation.Builder builder = net.sf.okapi.proto.GenericAnnotation.newBuilder();
		builder.setType(ann.getType());
		for (String n : ann.getNames()) {
			builder.putFields(n, toPrimitiveValue(ann.getValue(n)));
		}
		return builder.build();
	}

	private static net.sf.okapi.proto.PrimitiveValue toPrimitiveValue(Object value) {
		net.sf.okapi.proto.PrimitiveValue.Builder builder = net.sf.okapi.proto.PrimitiveValue.newBuilder();
		assert value != null;
		if (value instanceof String) {
			builder.setStringValue((String) value);
		} else if (value instanceof Integer) {
			builder.setIntValue((Integer) value);
		} else if (value instanceof Boolean) {
			builder.setBoolValue((Boolean) value);
		} else if (value instanceof Double) {
			builder.setDoubleValue((Double) value);
		} else {
			throw new IllegalArgumentException("Unsupported value type: " + value.getClass());
		}
		return builder.build();
    }

	static net.sf.okapi.proto.INameable toINameable(INameable value) {
		net.sf.okapi.proto.INameable.Builder builder = net.sf.okapi.proto.INameable.newBuilder().setResource(toIResource(value));

		builder.setIsTranslatable(value.isTranslatable())
				.setPreserveWhitespaces(value.preserveWhitespaces());
		if (value.getType() != null) {
			builder.setType(value.getType());
		}

		if (value.getMimeType() != null) {
			builder.setMimeType(value.getMimeType());
		}

		if (value.getName() != null) {
			builder.setName(value.getName());
		}

		return builder.build();
	}

	static net.sf.okapi.proto.IReferenceable toIReferenceable(IReferenceable value) {
		net.sf.okapi.proto.IReferenceable.Builder builder = net.sf.okapi.proto.IReferenceable.newBuilder()
				.setRefCount(value.getReferenceCount());

		return builder.build();
	}

	static net.sf.okapi.proto.TextFragment.TagType toTagType(TagType tt) {
		switch (tt) {
		case CLOSING:
			return net.sf.okapi.proto.TextFragment.TagType.CLOSING;
		case OPENING:
			return net.sf.okapi.proto.TextFragment.TagType.OPENING;
		case PLACEHOLDER:
			return net.sf.okapi.proto.TextFragment.TagType.PLACEHOLDER;
		default:
			return net.sf.okapi.proto.TextFragment.TagType.UNRECOGNIZED;
		}
	}

	static net.sf.okapi.proto.Code toCode(Code code, int position, boolean isolated) {
		net.sf.okapi.proto.Code.Builder builder = net.sf.okapi.proto.Code.newBuilder()
				.setTagType(toTagType(code.getTagType()))
				.setId(code.getId()).setCodeType(code.getType())
				.setData(code.getData())
				.setFlag(code.getFlag())
				.setPosition(position)
				.setIsolated(isolated)
				.setAdded(code.isAdded());

		// don't write outerData if it is null
		if (code.hasOuterData()) {
		    builder.setOuterData(code.getOuterData());
		}

		// correct isolated flag. If it's not a placeholder, it's not truly isolated
		// isolated can only be OPENING or CLOSING
		if (code.getTagType() == TagType.PLACEHOLDER) {
			builder.setIsolated(false);
		}
		if (null != code.getDisplayText()) {
			builder.setDisplayText(code.getDisplayText());
		}

		if (null != code.getOriginalId()) {
			builder.setOriginalId(code.getOriginalId());
		}
		
		if (null != code.getMergedData() && !code.getMergedData().isEmpty()) {
			builder.setMergedData(code.getMergedData());
		}

		if (code.hasAnnotation()) {
			for(String ann : code.getAnnotationsTypes()) {
				// normal properties handled below
				if (Code.PROPERTIES.equals(ann)) {
					continue;
				}
				builder.putInlineAnnotations(ann, code.getAnnotation(ann).toString());
			}
		}

		for (String propName : code.getPropertyNames()) {
			builder.addProperties(toProperty(code.getProperty(propName)));
		}

		return builder.build();
	}

	static net.sf.okapi.proto.TextFragment toTextFragment(TextFragment tf) {
		tf.balanceMarkers();
		net.sf.okapi.proto.TextFragment.Builder builder = net.sf.okapi.proto.TextFragment.newBuilder()
				.setText(tf.getText());

		if (tf.hasCode()) {
			tf.balanceMarkers();	
			List<Code> codes = tf.getCodes(); 
			int codeCount = 0;
			for ( int i=0; i<tf.length(); i++ ) {
				switch (tf.charAt(i)) {
				case TextFragment.MARKER_OPENING:
				case TextFragment.MARKER_CLOSING:
				case TextFragment.MARKER_ISOLATED:
					Code code = codes.get(TextFragment.toIndex(tf.charAt(++i)));
					int newPos = i - (codeCount*2); 
					builder.addCodes(toCode(code, newPos-1, tf.charAt(i - 1) == TextFragment.MARKER_ISOLATED));
					codeCount++;
					break;
				default:
					break;
				}
			}
		}
		return builder.build();
	}
	
	static net.sf.okapi.proto.Property toProperty(Property prop) {
		net.sf.okapi.proto.Property.Builder builder = net.sf.okapi.proto.Property.newBuilder()
				.setName(prop.getName())
				.setValue(prop.getValue() == null ? "" : prop.getValue())
				.addAllTypes(toPropertyTypes(prop.getTypes()))
				.setReadOnly(prop.isReadOnly());
		return builder.build();
	}

	static List<net.sf.okapi.proto.Property.PropertyType> toPropertyTypes(EnumSet<Property.Type> types) {
		List<net.sf.okapi.proto.Property.PropertyType> result = new ArrayList<>();
		for (Property.Type type : types) {
			result.add(net.sf.okapi.proto.Property.PropertyType.valueOf(type.name()));
		}
		return result;
	}


	static net.sf.okapi.proto.TextContainer toTextContainer(TextContainer tc, LocaleId locale) {
		net.sf.okapi.proto.TextContainer.Builder builder = net.sf.okapi.proto.TextContainer.newBuilder().
				setSegApplied(tc.hasBeenSegmented()).
				setNameable(toINameable(tc)).
				setLocale(locale == null ? "und" : locale.toBCP47());


		for (TextPart part : tc.getParts()) {
			builder.addParts(toTextPart(part));
		}

		NoteAnnotation na = tc.getAnnotation(NoteAnnotation.class);
		if (na != null) {
			for (Note note : na) {
				builder.addNotes(toNote(note));
			}
		}

		AltTranslationsAnnotation ats = tc.getAnnotation(AltTranslationsAnnotation.class);
		if (ats != null) {
			for (AltTranslation altTranslation : ats) {
				builder.addAltTrans(toAltTrans(altTranslation));
			}
		}
		return builder.build();
	}
	
	static net.sf.okapi.proto.TextPart toTextPart(TextPart part) {
		net.sf.okapi.proto.TextPart.Builder builder =
				net.sf.okapi.proto.TextPart.newBuilder()
						.setResource(toIResource(part))
						.setSegment(part.isSegment())
						.setWhitespaceStrategy(toWhitespaceStrategy(part.getWhitespaceStrategy()))
						.setText(toTextFragment(part.getContent()));

		if (part.getOriginalId() != null) {
			builder.setOriginalId(part.getOriginalId());
		}

		return builder.build();
	}

	static net.sf.okapi.proto.TextPart.WhitespaceStrategy toWhitespaceStrategy(WhitespaceStrategy whitespaceStrategy) {
		switch (whitespaceStrategy) {
			case INHERIT:
				return net.sf.okapi.proto.TextPart.WhitespaceStrategy.INHERIT;
			case NORMALIZE:
				return net.sf.okapi.proto.TextPart.WhitespaceStrategy.NORMALIZE;
			case PRESERVE:
				return net.sf.okapi.proto.TextPart.WhitespaceStrategy.PRESERVE;
			default:
				return net.sf.okapi.proto.TextPart.WhitespaceStrategy.UNRECOGNIZED;


		}
	}


	static net.sf.okapi.proto.Note toNote(Note note) {
		net.sf.okapi.proto.Note.Builder builder =
				net.sf.okapi.proto.Note.newBuilder().
						setNote(note.getNoteText()).
						setAnnotates(toAnnotates(note.getAnnotates() == null ? Note.Annotates.GENERAL :
							note.getAnnotates())).
						setPriority(toPriority(note.getPriority() == null ? Note.Priority.ONE : note.getPriority())).
						setFrom(note.getFrom() == null ? "" : note.getFrom()).
						setXmlLang(note.getXmLang() == null ? "" : note.getXmLang());

		return builder.build();
	}

	static net.sf.okapi.proto.AltTranslation.MatchType toMatchType(MatchType matchType) {
		switch (matchType) {
			case ACCEPTED: return net.sf.okapi.proto.AltTranslation.MatchType.ACCEPTED;
			case EXACT_UNIQUE_ID: return net.sf.okapi.proto.AltTranslation.MatchType.EXACT_UNIQUE_ID;
			case EXACT_LOCAL_CONTEXT: return net.sf.okapi.proto.AltTranslation.MatchType.EXACT_LOCAL_CONTEXT;
			case EXACT: return net.sf.okapi.proto.AltTranslation.MatchType.EXACT;
			case EXACT_TEXT_ONLY: return net.sf.okapi.proto.AltTranslation.MatchType.EXACT_TEXT_ONLY;
			case FUZZY: return net.sf.okapi.proto.AltTranslation.MatchType.FUZZY;
			default: return net.sf.okapi.proto.AltTranslation.MatchType.UKNOWN;
		}
	}

	static net.sf.okapi.proto.AltTranslation toAltTrans(AltTranslation altTrans) {
		net.sf.okapi.proto.AltTranslation.Builder builder =
				net.sf.okapi.proto.AltTranslation.newBuilder().
						setTextUnit(toTextUnit(altTrans.getTextUnit(), altTrans.getSourceLocale())).
						setSourceLocale(altTrans.getSourceLocale().toBCP47()).
						setTargetLocale(altTrans.getTargetLocale().toBCP47()).
						setType(toMatchType(altTrans.getType())).
						setCombinedScore(altTrans.getCombinedScore()).
						setOrigin(altTrans.getOrigin());

		if (altTrans.getEngine() != null) {
			builder.setEngine(altTrans.getEngine());
		}
		builder.setFromOriginal(altTrans.getFromOriginal());
		builder.setFuzzyScore(altTrans.getFuzzyScore());
		builder.setQualityScore(altTrans.getQualityScore());
		builder.setAltTransType(altTrans.getALttransType());

		return builder.build();
	}

	static net.sf.okapi.proto.Note.Annotates toAnnotates(Note.Annotates annotates) {
		switch (annotates) {
			case SOURCE: return net.sf.okapi.proto.Note.Annotates.SOURCE;
			case TARGET: return net.sf.okapi.proto.Note.Annotates.TARGET;
			default: return net.sf.okapi.proto.Note.Annotates.GENERAL;
		}
	}

	static net.sf.okapi.proto.Note.Priority toPriority(Note.Priority priority) {
		switch (priority) {
			case ONE: return net.sf.okapi.proto.Note.Priority.ONE;
			case TWO: return net.sf.okapi.proto.Note.Priority.TWO;
			case THREE: return net.sf.okapi.proto.Note.Priority.THREE;
			case FOUR: return net.sf.okapi.proto.Note.Priority.FOUR;
			case FIVE: return net.sf.okapi.proto.Note.Priority.FIVE;
			case SIX: return net.sf.okapi.proto.Note.Priority.SIX;
			case SEVEN: return net.sf.okapi.proto.Note.Priority.SEVEN;
			case EIGHT: return net.sf.okapi.proto.Note.Priority.EIGHT;
			case NINE: return net.sf.okapi.proto.Note.Priority.NINE;
			default: return net.sf.okapi.proto.Note.Priority.TEN;
		}
	}

	static net.sf.okapi.proto.TextUnit toTextUnit(ITextUnit tu, LocaleId sourceLocale) {
		net.sf.okapi.proto.TextUnit.Builder builder = net.sf.okapi.proto.TextUnit.newBuilder()				
				.setSource(toTextContainer(tu.getSource(), sourceLocale))
				.setNameable(toINameable(tu))
				.setReferenceable(toIReferenceable(tu));
		
		Map<String, net.sf.okapi.proto.TextContainer> targets = new HashMap<>();
		for (LocaleId locale : tu.getTargetLocales()) {
			targets.put(locale.toBCP47(), toTextContainer(tu.getTarget(locale), locale));
		}
		builder.putAllTargets(targets);

		NoteAnnotation na = tu.getAnnotation(NoteAnnotation.class);
		if (na != null) {
			for (Note note : na) {
				builder.addNotes(toNote(note));
			}
		}

		return builder.build();
	}

	static net.sf.okapi.proto.StartDocument toStartDocument(StartDocument sdoc) {
		net.sf.okapi.proto.StartDocument.Builder builder = net.sf.okapi.proto.StartDocument.newBuilder()
				.setNameable(toINameable(sdoc))
				.setHasUtf8Bom(sdoc.hasUTF8BOM())
				.setMultilingual(sdoc.isMultilingual());

		if (null != sdoc.getEncoding()) {
			builder.setEncoding(sdoc.getEncoding());
		}

		if (null != sdoc.getLineBreak()) {
			builder.setLineBreak(sdoc.getLineBreak());
		}

		if (null != sdoc.getLocale()) {
			builder.setLocale(sdoc.getLocale().toBCP47());
		}

		if (null != sdoc.getFilterParameters()) {
			builder.setParameters(sdoc.getFilterParameters().toString());
		}

		return builder.build();
	}

	static net.sf.okapi.proto.EndDocument toEndDocument(Ending value) {
		net.sf.okapi.proto.EndDocument.Builder builder = net.sf.okapi.proto.EndDocument.newBuilder()
				.setResource(toIResource(value));
		return builder.build();
	}

	static net.sf.okapi.proto.EndGroup toEndGroup(Ending value) {
		net.sf.okapi.proto.EndGroup.Builder builder = net.sf.okapi.proto.EndGroup.newBuilder()
				.setResource(toIResource(value));
		return builder.build();
	}

	static net.sf.okapi.proto.StartGroup toStartGroup(StartGroup value) {
		net.sf.okapi.proto.StartGroup.Builder builder = net.sf.okapi.proto.StartGroup.newBuilder()
				.setNameable(toINameable(value))
				.setReferenceable(toIReferenceable(value));

		String parentId = value.getParentId();
		if (null != parentId)
			builder.setParentId(parentId);

		return builder.build();
	}

	static net.sf.okapi.proto.StartSubfilter toStartSubfilter(StartSubfilter value) {
		net.sf.okapi.proto.StartSubfilter.Builder builder = net.sf.okapi.proto.StartSubfilter.newBuilder()
				.setNameable(toINameable(value))
				.setStartDoc(toStartDocument(value.getStartDoc()));
		return builder.build();
	}

	static net.sf.okapi.proto.EndSubfilter toEndSubfilter(EndSubfilter value) {
		net.sf.okapi.proto.EndSubfilter.Builder builder = net.sf.okapi.proto.EndSubfilter.newBuilder()
				.setResource(toIResource(value));
		return builder.build();
	}

	static net.sf.okapi.proto.EndSubDocument toEndSubDocument(Ending value) {
		net.sf.okapi.proto.EndSubDocument.Builder builder = net.sf.okapi.proto.EndSubDocument.newBuilder()
				.setResource(toIResource(value));
		return builder.build();
	}

	static net.sf.okapi.proto.StartSubDocument toStartSubDocument(StartSubDocument value) {
		net.sf.okapi.proto.StartSubDocument.Builder builder = net.sf.okapi.proto.StartSubDocument.newBuilder()
				.setNameable(toINameable(value));

		String parentId = value.getParentId();
		if (null != parentId)
			builder.setParentId(parentId);

		String filterIdId = value.getFilterId();
		if (null != filterIdId)
			builder.setFilterId(filterIdId);

		return builder.build();
	}

	public static net.sf.okapi.proto.Event toEvent(Event event, LocaleId sourceLocale) {
		net.sf.okapi.proto.Event.Builder builder = net.sf.okapi.proto.Event.newBuilder();

		if (event.isStartDocument()) {
			builder.setStartDocument(toStartDocument(event.getStartDocument()));
		} else if (event.isEndDocument()) {
			builder.setEndDocument(toEndDocument(event.getEnding()));
		} else if (event.isTextUnit()) {
			builder.setTextUnit(toTextUnit(event.getTextUnit(), sourceLocale));
		} else if (event.isStartGroup()) {
			builder.setStartGroup(toStartGroup(event.getStartGroup()));
		} else if (event.isEndGroup()) {
			builder.setEndGroup(toEndGroup(event.getEnding()));
		} else if (event.isStartSubfilter()) {
			builder.setStartSubfilter(toStartSubfilter(event.getStartSubfilter()));
		} else if (event.isEndSubfilter()) {
			builder.setEndSubfilter(toEndSubfilter(event.getEndSubfilter()));
		} else if (event.isStartSubDocument()) {
			builder.setStartSubDocument(toStartSubDocument(event.getStartSubDocument()));
		} else if (event.isEndSubDocument()) {
			builder.setEndSubDocument(toEndSubDocument(event.getEnding()));
		}

		return builder.build();
	}

	public static net.sf.okapi.proto.Events toEvents(List<Event> events, LocaleId sourceLocale) {
		net.sf.okapi.proto.Events.Builder builder = net.sf.okapi.proto.Events.newBuilder();
		for (Event e : events) {
			builder.addEvents(toEvent(e, sourceLocale));
		}
		return builder.build();
	}
}
