package net.sf.okapi.lib.merge.merge;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filterwriter.XLIFFWriter;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xmlunit.matchers.CompareMatcher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnit4.class)
public class XLIFFFilterNamespacePrefixTest {

    private XLIFFFilter filter;
    private LocaleId locEN = LocaleId.fromString("en");
    private LocaleId locDE = LocaleId.fromString("de");

    @Before
    public void setUp () {
        filter = new XLIFFFilter();
    }

    @Test
    public void shouldKeepNamespacePrefixOfXTag() throws Exception {
        ensureResultContainsNamespacePrefixesAsInput("Foo<xlf:x id=\"1\"/>Bar");
    }

    @Test
    public void shouldKeepNamespacePrefixOfGTag() throws Exception {
        ensureResultContainsNamespacePrefixesAsInput("<xlf:g id=\"1\">Foo Bar</xlf:g>");
    }

    @Test
    public void shouldKeepNamespacePrefixOfBxExTags() throws Exception {
        ensureResultContainsNamespacePrefixesAsInput("<xlf:bx id=\"1\"/>Foo Bar<xlf:ex id=\"1\"/>");
    }

    @Test
    public void shouldKeepNamespacePrefixOfBptEptTags() throws Exception {
        ensureResultContainsNamespacePrefixesAsInput("<xlf:bpt id=\"1\">bpt-data</xlf:bpt>Foo Bar<xlf:ept id=\"1\">ept-data</xlf:ept>");
    }

    @Test
    public void testUseTranslationTargetStateWhenPresent() throws Exception {
        String input = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
                + "<xliff xmlns:xlf=\"urn:oasis:names:tc:xliff:document:1.2\" version=\"1.2\">\r"
                + "<xlf:file source-language=\"de\" target-language=\"en\" datatype=\"x-test\" original=\"file.ext\">"
                + "<xlf:body>"
                + "<xlf:trans-unit id=\"1\">"
                + "<xlf:source>de</xlf:source>"
                + "<xlf:target state = \"abc\">de</xlf:target>"
                + "</xlf:trans-unit>"
                + "</xlf:body>"
                + "</xlf:file>"
                + "</xliff>";



        String translation = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
                + "<xliff xmlns:xlf=\"urn:oasis:names:tc:xliff:document:1.2\" version=\"1.2\">\r"
                + "<xlf:file source-language=\"de\" target-language=\"en\" datatype=\"x-test\" original=\"file.ext\">"
                + "<xlf:body>"
                + "<xlf:trans-unit id=\"1\">"
                + "<xlf:source>de</xlf:source>"
                + "<xlf:target state = \"xyz\">en</xlf:target>"
                + "</xlf:trans-unit>"
                + "</xlf:body>"
                + "</xlf:file>"
                + "</xliff>";
        String actual = mergeTranslation(input, translation, true);

        assertThat(translation, CompareMatcher.isIdenticalTo(actual));
    }


    @Test
    public void testUseTranslationTargetStateWhenAbsent() throws Exception {
        String input = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
                + "<xliff xmlns:xlf=\"urn:oasis:names:tc:xliff:document:1.2\" version=\"1.2\">\r"
                + "<xlf:file source-language=\"de\" target-language=\"en\" datatype=\"x-test\" original=\"file.ext\">"
                + "<xlf:body>"
                + "<xlf:trans-unit id=\"1\">"
                + "<xlf:source>target</xlf:source>"
                + "<xlf:target>target</xlf:target>"
                + "</xlf:trans-unit>"
                + "</xlf:body>"
                + "</xlf:file>"
                + "</xliff>";



        String translation = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
                + "<xliff xmlns:xlf=\"urn:oasis:names:tc:xliff:document:1.2\" version=\"1.2\">\r"
                + "<xlf:file source-language=\"de\" target-language=\"en\" datatype=\"x-test\" original=\"file.ext\">"
                + "<xlf:body>"
                + "<xlf:trans-unit id=\"1\">"
                + "<xlf:source>target</xlf:source>"
                + "<xlf:target state = \"xyz\">target2</xlf:target>"
                + "</xlf:trans-unit>"
                + "</xlf:body>"
                + "</xlf:file>"
                + "</xliff>";
        String actual = mergeTranslation(input, translation, true);

        assertThat(translation, CompareMatcher.isIdenticalTo(actual));
    }

    private void ensureResultContainsNamespacePrefixesAsInput(String content) throws Exception {
        String input = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r"
                + "<xliff xmlns:xlf=\"urn:oasis:names:tc:xliff:document:1.2\" version=\"1.2\">\r"
                + "<xlf:file source-language=\"de\" target-language=\"en\" datatype=\"x-test\" original=\"file.ext\">"
                + "<xlf:body>"
                + "<xlf:trans-unit id=\"1\">"
                + "<xlf:source>" + content + "</xlf:source>"
                + "<xlf:target>" + content + "</xlf:target>"
                + "</xlf:trans-unit>"
                + "</xlf:body>"
                + "</xlf:file>"
                + "</xliff>";

        String translation = createXliffTranslation(input);
        String actual = mergeTranslation(input, translation, false);

        assertThat(input, CompareMatcher.isIdenticalTo(actual));
    }

    private String createXliffTranslation(String input) throws IOException {
        ArrayList<Event> events = getEvents(input);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        XLIFFWriter xliffWriter = new XLIFFWriter();
        xliffWriter.getParameters().setPlaceholderMode(false);
        xliffWriter.getParameters().setIncludeCodeAttrs(true);
        xliffWriter.setOutput(outputStream);
        xliffWriter.setOptions(locEN, null);
        for (Event event : events) {
            xliffWriter.handleEvent(event);
        }
        xliffWriter.close();
        outputStream.close();

        return new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
    }

    private String mergeTranslation(String input, String translation, boolean useTranslationTargetState) {
        ByteArrayOutputStream resultStream = new ByteArrayOutputStream();

        XLIFFFilter originalFilter = new XLIFFFilter();
        originalFilter.getParameters().setUseTranslationTargetState(useTranslationTargetState);
        originalFilter.open(new RawDocument(input, locDE, locEN));

        try (SkeletonMergerWriter skelMergerWriter = new SkeletonMergerWriter(originalFilter, null, null)) {
            skelMergerWriter.setOptions(locEN, "UTF-8");
            skelMergerWriter.setOutput(resultStream);

            try (XLIFFFilter translationFilter = new XLIFFFilter()) {
                originalFilter.getParameters().setUseTranslationTargetState(useTranslationTargetState);
                translationFilter
                        .open(new RawDocument(translation, LocaleId.GERMAN, LocaleId.ENGLISH));
                while (translationFilter.hasNext()) {
                    Event next = translationFilter.next();
                    skelMergerWriter.handleEvent(next);
                }
            }
        }

        return new String(resultStream.toByteArray(), StandardCharsets.UTF_8);
    }

    private ArrayList<Event> getEvents(String snippet) {
        return getEvents(snippet, filter);
    }

    private ArrayList<Event> getEvents (String snippet,
            XLIFFFilter filterToUse) {
        return FilterTestDriver.getEvents(filterToUse, snippet, locDE, locEN);
    }
}
