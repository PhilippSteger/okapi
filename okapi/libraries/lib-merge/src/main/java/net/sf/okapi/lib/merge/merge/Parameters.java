/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.merge.merge;

import java.util.Arrays;
import java.util.Optional;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	static final String APPROVEDONLY = "approvedOnly"; //$NON-NLS-1$
	static final String THROW_EXCEPTION_CODE_DIFFERENCES = "throwExceptionForCodeDifferences"; //$NON-NLS-1$
	static final String THROW_EXCEPTION_SEGMENT_ID_DIFFERENCES = "throwExceptionForSegmentIdDifferences"; //$NON-NLS-1$
	static final String ADD_MISSING = "ADDMISSING"; //$NON-NLS-1$
	static final String COPY_SOURCE_IF_NON_TEXTUAL = "copySourceIfNonTextual";
	static final String MERGE_NOTES = "mergeNotes";

	public enum NoteMergeStrategy {
		/**
		 * Discard notes added in translation, copy any notes present in source.
		 * (Legacy behavior.)
		 */
		DISCARD(0),

		/**
		 * Add any notes added in translation to those present in the source.
		 * (May result in duplication if notes are modified in translation,
		 * as there is no stable id for note data.)
		 */
		MERGE(1),

		/**
		 * Include any notes present in translation and discard those that
		 * were present in the source.
		 */
		REPLACE(2);

		private final int value;
		NoteMergeStrategy(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
		public static Optional<NoteMergeStrategy> byValue(int value) {
			return Arrays.stream(values()).filter(type -> type.value == value).findFirst();
		}
	}

	public Parameters () {
		super();
	}

	@Override
	public void reset () {
		super.reset();
		setApprovedOnly(false);
		setThrowCodeException(false);
		setThrowSegmentIdException(true);
		setAddMissing(false);
		setCopySourceIfNonTextual(true);
		setNoteMergeStrategy(NoteMergeStrategy.DISCARD);
	}

	/**
	 * Only merge approved translations. 
	 * @return boolean - use approved target translations only?
	 */
	public boolean isApprovedOnly() {
		return getBoolean(APPROVEDONLY);
	}

	public void setApprovedOnly(boolean approvedOnly) {
		setBoolean(APPROVEDONLY, approvedOnly);
	}

	public boolean isThrowCodeException() {
		return getBoolean(THROW_EXCEPTION_CODE_DIFFERENCES);
	}

	public void setThrowCodeException(boolean exception) {
		setBoolean(THROW_EXCEPTION_CODE_DIFFERENCES, exception);
	}
	
	public boolean isThrowSegmentIdException() {
		return getBoolean(THROW_EXCEPTION_SEGMENT_ID_DIFFERENCES);
	}

	public void setThrowSegmentIdException(boolean exception) {
		setBoolean(THROW_EXCEPTION_SEGMENT_ID_DIFFERENCES, exception);
	}

	public boolean isAddMissing() {
		return getBoolean(ADD_MISSING);
	}

	public void setAddMissing(boolean addMissing) {
		setBoolean(ADD_MISSING, addMissing);
	}

	public boolean isCopySourceIfNonTextual() {
		return getBoolean(COPY_SOURCE_IF_NON_TEXTUAL);
	}

	public void setCopySourceIfNonTextual(boolean copySource) { setBoolean(COPY_SOURCE_IF_NON_TEXTUAL, copySource); }

	public void setNoteMergeStrategy(NoteMergeStrategy noteMergeStrategy) {
		setInteger(MERGE_NOTES, noteMergeStrategy.value);
	}

	public NoteMergeStrategy getNoteMergeStrategy() {
		return NoteMergeStrategy.byValue(getInteger(MERGE_NOTES)).orElse(NoteMergeStrategy.DISCARD);
	}
}
