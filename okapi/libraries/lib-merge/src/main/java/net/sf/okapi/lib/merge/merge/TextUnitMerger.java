/*===========================================================================
Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.merge.merge;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Range;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiMergeException;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.CodeMatchStrategy;
import net.sf.okapi.common.resource.CodeMatches;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragmentUtil;
import net.sf.okapi.common.resource.TextPart;
import net.sf.okapi.common.resource.TextPartComparator;
import net.sf.okapi.common.resource.TextUnitUtil;
import org.incava.diff.LCS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class TextUnitMerger implements ITextUnitMerger {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private LocaleId trgLoc;
	private Parameters params;
	private String translatedTuId;
	private CodeMatchStrategy strategy;

	public TextUnitMerger() {
		params = new Parameters();
		this.strategy = CodeMatchStrategy.STRICT;
	}

	@Override
	public ITextUnit mergeTargets(final ITextUnit tuFromSkel, final ITextUnit tuFromTran) {
		if (tuFromSkel == null || tuFromTran == null) {
			LOGGER.warn("Null TextUnit in TextUnitMerger.");
			return tuFromSkel;
		}

		translatedTuId = tuFromTran.getId();

		if ( !tuFromSkel.getId().equals(tuFromTran.getId()) ) {
			final String s = String.format("Text Unit id mismatch during merger: Original id=\"%s\" target id=\"%s\"", tuFromSkel.getId(), tuFromTran.getId());
			LOGGER.error(s);
			if (params.isThrowSegmentIdException()) {
				throw new OkapiMergeException(s);
			}
		}

		// create merged TextUnit. This should be the original reference
		// from tuFromSkel as this reference may be used across events
		final ITextUnit mergedTextUnit = tuFromSkel;

		// clone tuFromSkel so we can copy without conflicting
		// with mergedTextUnit (same reference as tuFromSkel)
		final ITextUnit tuFromSkelClone = tuFromSkel.clone();

		// copy the source if there is no target and the content is non-translatable
		// likely this is a reference-only text unit
		if (params.isCopySourceIfNonTextual() &&
				!tuFromSkel.getSource().hasText(true) &&
				(tuFromTran.getTarget(trgLoc) == null || tuFromTran.getTarget(trgLoc).isEmpty())) {
			mergedTextUnit.setTarget(trgLoc, tuFromSkel.getSource().clone());
			return mergedTextUnit;
		}

		// Check if we have a translation
		// if not we merge with empty target
		if (!tuFromTran.hasTarget(trgLoc) || tuFromTran.getTarget(trgLoc) == null) {
			LOGGER.warn("No translation found for TU id='{}'.", tuFromTran.getId());
			return mergedTextUnit;
		}

		// Process the "approved" property
		boolean isTransApproved = false;
		final Property traProp = tuFromTran.getTarget(trgLoc).getProperty(Property.APPROVED);
		if ( traProp != null ) {
			isTransApproved = traProp.getValue().equals("yes");
		}
		if (params != null && params.isApprovedOnly() && !isTransApproved) {
			LOGGER.warn("Item id='{}': Target is not approved. Using empty translation.", tuFromSkel.getId());
			mergedTextUnit.setTarget(trgLoc, null);
			return mergedTextUnit;
		}

		// NOTE: since we tested target for null above, no need to check for null below


		// need source from translated tu since segmentation, properties,
		// codes or annotations could have changed post filter
		// For example xliff2 filter may deepen its segmentation or
		// new properties/annotations were added
		mergedTextUnit.setSource(tuFromTran.getSource().clone());

		// clone the translated target
		mergedTextUnit.setTarget(trgLoc, tuFromTran.getTarget(trgLoc).clone());

		// we need to copy over container properties/annotations from tuFromSkel
		// as some of these may be lost during translation
		IResource.copy(tuFromSkelClone.getSource(), mergedTextUnit.getSource());
		IResource.copy(tuFromSkelClone.getTarget(trgLoc), mergedTextUnit.getTarget(trgLoc));

		// We need to preserve the original segmentation for merging
		// Some segmented formats like xliff2 allow post-segmentation
		// which will cause mismatches with the original. These must be joined.
		final boolean segmentationSupported = MimeTypeMapper.isSegmentationSupported(tuFromSkelClone.getMimeType());
		final boolean keepCodeIds = true;

		// Remember the ranges to set them back after joining, may be empty list
		// must join all segments to match codes.
		// bilingual formats like xliff should preserve their code ids as they must match with any targets
		List<Range> srcRanges = null;
		List<Range> trgRanges = null;
		if (!mergedTextUnit.getSource().contentIsOneSegment()) {
			srcRanges = mergedTextUnit.getSource().getSegments().getRanges(keepCodeIds);
		}
		if (!mergedTextUnit.getTarget(trgLoc).contentIsOneSegment()) {
			trgRanges = mergedTextUnit.getTarget(trgLoc).getSegments().getRanges(keepCodeIds);
		}

		// if there are any codes that were simplified (merged or trimmed)
		// recover the original code data now.
		// this happens in TextUnitUtil.simplifyCodesPostSegmentation
		for (final TextPart tp : mergedTextUnit.getSource().getSegments()) {
			if (TextUnitUtil.hasMergedCode(tp.text)) {
				tp.text = TextUnitUtil.expandCodes(tp.text);
			}
		}

		for (final TextPart tp : mergedTextUnit.getTarget(trgLoc).getSegments()) {
			if (TextUnitUtil.hasMergedCode(tp.text)) {
				tp.text = TextUnitUtil.expandCodes(tp.text);
			}
		}

		// join the segments together for the code transfer
		// This allows to move codes anywhere in the text unit,
		// not just each part. We do remember the ranges because
		// some formats will require to be merged with segments
		if (!mergedTextUnit.getSource().contentIsOneSegment()) {
			mergedTextUnit.getSource().getSegments().joinAll(keepCodeIds);
		}
		if (!mergedTextUnit.getTarget(trgLoc).contentIsOneSegment()) {
			mergedTextUnit.getTarget(trgLoc).getSegments().joinAll(keepCodeIds);
		}

		// remove outerData from translated codes as this is often a byproduct of the translated
		// file. example xliff, xliff2 etc.. Some translated files may not introduce outerData
		// such as custom JSON formats
		removeOuterData(mergedTextUnit.getSource().getFirstContent().getCodes());
		removeOuterData(mergedTextUnit.getTarget(trgLoc).getFirstContent().getCodes());

		TextContainer skelSource = tuFromSkelClone.getSource();
		TextContainer skelTarget = tuFromSkelClone.getTarget(trgLoc);
		// we need a copy of source skel to match parts in mergedTextUnit
		// don't join if only one segment as this may destroy metadata
		if (!skelSource.contentIsOneSegment()) {
			skelSource = tuFromSkelClone.getSource().clone();
			skelSource.getSegments().joinAll(keepCodeIds);
		}

		// we need a copy of target skel to match parts in mergedTextUnit
		// don't join if only one segment as this may destroy metadata
		if (skelTarget != null && !skelTarget.contentIsOneSegment()) {
			skelTarget = tuFromSkelClone.getTarget(trgLoc).clone();
			skelTarget.getSegments().joinAll(keepCodeIds);
		}

		// copy code fields from original/skeleton tu. We do this because
		// we use the translated source (i.e, xliff) and info may be lost in
		// the translated file
		copyCodeMetadata(skelSource.getFirstContent(), mergedTextUnit.getSource().getFirstContent());

		// now copy code fields to the target so source and target codes are the same
		// but only for those that match. We do not overwrite existing target values
		copyCodeMetadata(mergedTextUnit.getSource().getFirstContent(), mergedTextUnit.getTarget(trgLoc).getFirstContent());

		// if there was an original target do the same so we catch added codes in the target
		if (skelTarget != null && skelTarget.hasCode()) {
			copyCodeMetadata(skelTarget.getFirstContent(), mergedTextUnit.getTarget(trgLoc).getFirstContent());
		}

		// some mergers can't handle segmented content - otherwise we could always just keep the segmented versions
		// Re-apply segmentation ranges if the format allows segments
		if (srcRanges != null && segmentationSupported) {
			mergedTextUnit.getSource().getSegments().create(srcRanges, true,
					ISegments.MetaCopyStrategy.IDENTITY);
		}
		if (trgRanges != null && segmentationSupported) {
			mergedTextUnit.getTarget(trgLoc).getSegments().create(trgRanges, true,
					ISegments.MetaCopyStrategy.IDENTITY);
		}

		// Check if source/target segment count is the same
		if (mergedTextUnit.getSource().getSegments().count() != mergedTextUnit.getTarget(trgLoc).getSegments().count()) {
			LOGGER.warn("Item id='{}': Different number of source and target segments.", tuFromTran.getId());
		}

		// we need to copy over any lost TextPart/Segment properties/annotations deleted with joinAll()
		// this is mostly for the xliff2 filter but the code should stay generic
		// FIXME: We may need to handle non-mergeAsSegments properties/annotations copy
		//  since some non-segment based filters may add TextPart level properties/annotations in the future.
		//  Currently I don't think this is the case.
		copyTextPartMetadata(tuFromSkelClone.getSource().getParts(), mergedTextUnit.getSource().getParts(),
					mergedTextUnit.getId());
			// if there was an original target from skeleton
		if (tuFromSkelClone.hasTarget(trgLoc)) {
			copyTextPartMetadata(tuFromSkelClone.getTarget(trgLoc).getParts(),
					mergedTextUnit.getTarget(trgLoc).getParts(), mergedTextUnit.getId());
		}
		mergedTextUnit.getSource().setHasBeenSegmentedFlag(tuFromTran.getSource().hasBeenSegmented());
		mergedTextUnit.getTarget(trgLoc).setHasBeenSegmentedFlag(tuFromTran.getTarget(trgLoc).hasBeenSegmented());

		return mergedTextUnit;
	}

	@Override
	public void copyTextPartMetadata(final List<TextPart> from, final List<TextPart> to, final String id) {
		// short circuit optimization
		if (Util.isEmpty(from) || Util.isEmpty(to)) {
			return;
		}

		final TextPartComparator cmp = new TextPartComparator();
		final boolean [] matched = new boolean[to.size()];
		Arrays.fill(matched, false);

		// align TextParts using "Longest Common Sequence"
		// this will give a more accurate match for TextParts without id's
		// as some TextParts have only whitespace for comparison
		final LCS<TextPart> diff = new LCS<>(from, to, cmp);
		final List<Integer> matches = diff.getMatches();

		int n = -1;
		for (final Integer m : matches) {
			TextPart toPart;
			n++;
			final TextPart origPart = from.get(n);
			if (m == null) {
				// LCS can return mismatches if heavily reordered
				// resort to linear search
				toPart = Util.findMatch(origPart, to, matched, cmp);

				if (toPart == null) {
					// log error only if there was metadata in the original file TextPart
					// that needed to be copied, otherwise assume segmentation changes
					// in the translated file
					if (hasMeta(origPart)) {
						LOGGER.error("TextUnit id='{}' TextPart id='{}': Can't find matching TextPart in merged text unit. "
										+ "Cannot copy TextPart properties/annotations. Merged file may differ from original", id,
								origPart.id);
					} else {
						LOGGER.warn("TextUnit id='{}' TextPart id='{}': Can't find matching TextPart in merged text unit. "
								+ "Possible segmentation changes in the translated file", id, origPart.id);
					}
					continue;
				}
			} else {
				toPart = to.get(m);
				matched[m] = true;
			}

			toPart.id = origPart.id;
			toPart.originalId = origPart.originalId;
			toPart.whitespaceStrategy = origPart.whitespaceStrategy;
			IResource.copy(origPart, toPart);
		}
	}

	public <T extends TextPart> boolean hasMeta(final T p) {
		final boolean hasProps = !p.getPropertyNames().isEmpty();
		final boolean hasAnnotations = p.getAnnotations().iterator().hasNext();
		return (hasProps || hasAnnotations || p.id != null);
	}
	@Override
	public void copyCodeMetadata(final TextFragment from, final TextFragment to) {
		CodeMatches m = TextFragmentUtil.alignAndCopyCodeMetadata(from, to, params.isAddMissing(), false, strategy);
		if (m.hasFromMismatch(false) || m.hasToMismatch(false)) {
			TextFragmentUtil.logCodeMismatchErrors(m, from, to, translatedTuId);
			if (params.isThrowCodeException()) {
				throw new OkapiMergeException("TextUnit Merge Error for TU: " + translatedTuId);
			}
		}
	}

	private void removeOuterData(final List<Code> codes) {
		for (final Code c : codes) {
			c.setOuterData(null);
		}
	}

	@Override
	public void setTargetLocale(final LocaleId trgLoc) {
		this.trgLoc = trgLoc;
	}

	public LocaleId getTargetLocale() {
		return trgLoc;
	}

	@Override
	public Parameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(final Parameters params) {
		this.params = params;
	}

	@Override
	public void setCodeMatchStrategy(CodeMatchStrategy strategy) {
		this.strategy = strategy;
	}
}
