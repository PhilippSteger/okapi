/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TagalogSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("tl");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		test("Ang Linux ay isang operating system kernel para sa mga operating system na humahalintulad sa Unix.",
				" Isa ang Linux sa mga pinaka-prominanteng halimbawa ng malayang software at pagsasagawa ng open source; "
						+ "madalas, malayang mapapalitan, gamitin, at maipamahagi ninuman ang "
						+ "lahat ng pinag-ugatang source code (pinagmulang kodigo).");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}
