/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LithuanianSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("lt");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		test("Linux – laisvos operacinės sistemos branduolio (kernel) pavadinimas.",
				" Dažnai taip sutrumpintai vadinama ir bendrai visa Unix-tipo operacinė sistema naudojanti Linux branduolį kartu su sisteminėmis programomis bei bibliotekomis iš GNU projekto.");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}
