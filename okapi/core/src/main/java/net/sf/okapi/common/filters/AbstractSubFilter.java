package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.resource.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractSubFilter extends AbstractFilter implements ISubFilter {
    private String parentType;
    private StartSubfilter startSubFilter;
    private String parentName;
    private String parentId;
    private int sectionIndex;
    private IEncoder parentEncoder;
    private EndSubfilter endSubFilter;
    private SubFilterEventConverter converter;

    public AbstractSubFilter() {
        super();
    }

    protected Event convertEvent(Event event) {
        if (getConverter() != null) {
            return getConverter().convertEvent(event);
        }
        return event;
    }

    @Override
    public List<Event> getEvents(RawDocument input) {
        List<Event> events = new LinkedList<>();
        open(input);
        while (hasNext()) {
            events.add(next());
        }
        close();
        return Collections.unmodifiableList(events);
    }

    @Override
    public List<Event> getEvents(ITextUnit tu, LocaleId sourceLocale, LocaleId targetLocale) {
        String text = tu.getSource().getCodedText();
        List<Event> events = new LinkedList<>();
        try (RawDocument doc = new RawDocument(text, sourceLocale, targetLocale)) {
            open(doc);
            setParentId(tu.getId());
            setParentName(tu.getName());
            setParentType(tu.getType());
            while (hasNext()) {
                Event e = next();
                if (e.isTextUnit()) {
                    ITextUnit stu = e.getTextUnit();
                    // FIXME: initial implementation. We need a merge option for standard annotations
                    //  like net.sf.okapi.filters.xliff2.model.XLIFF2NotesAnnotation
                    IWithProperties.copy(tu, stu);
                    IWithAnnotations.copy(tu, stu);
                }
                events.add(e);
            }
            close();
        }
        return Collections.unmodifiableList(events);
    }

    /**
     * @return
     */
    @Override
    public StartSubfilter getStartSubfilter() {
        return startSubFilter;
    }

    /**
     * @param startSubfilter
     */
    @Override
    public void setStartSubfilter(StartSubfilter startSubfilter) {
        this.startSubFilter = startSubfilter;
    }

    /**
     * @return
     */
    @Override
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName
     */
    @Override
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    /**
     * @return
     */
    @Override
    public int getSectionIndex() {
        return sectionIndex;
    }

    /**
     * @param sectionIndex
     */
    @Override
    public void setSectionIndex(int sectionIndex) {
        this.sectionIndex = sectionIndex;
    }

    @Override
    public String getParentType() {
        return parentType;
    }

    /**
     * @param parentType
     */
    @Override
    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    /**
     * @param parentEncoder
     */
    @Override
    public void setParentEncoder(IEncoder parentEncoder) {
        this.parentEncoder = parentEncoder;
    }

    /**
     * @param endSubfilter
     */
    @Override
    public void setEndSubfilter(EndSubfilter endSubfilter) {
        this.endSubFilter = endSubfilter;
    }


    @Override
    public void setConverter(SubFilterEventConverter converter) {
        this.converter = converter;
    }

    @Override
    public SubFilterEventConverter getConverter() {
        return converter;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    @Override
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
