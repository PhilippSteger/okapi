package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.encoder.IEncoder;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.ITextUnit;

import java.util.stream.Stream;

public class SubFilterWrapperStep extends BasePipelineStep implements IPipelineStep {
    private final ISubFilter subFilter;
    private int sectionIndex = 1;

    public SubFilterWrapperStep(IFilter filter, IEncoder parentEncoder) {
        this.subFilter = ISubFilter.create(filter, parentEncoder);
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String getDescription() {
        return "Wrapper around a SubFilter that produces flat results";
    }

    @Override
    public Stream<Event> handleStream(Event event) {
        try {
            if (event.isTextUnit()) {
                ITextUnit tu = event.getTextUnit();
                subFilter.setSectionIndex(sectionIndex++);
                var events = subFilter.getEvents(tu, getSourceLocale(), getTargetLocale());
                return Stream.of(events.toArray(new Event[0]));
            }
        } catch(Exception e) {
            throw new OkapiException(
                    String.format("Error in subfilter: %s processing %s", subFilter.getName(), event.toString()), e);
        } finally{
            subFilter.close();
        }

        return Stream.of(event);
    }

    @Override
    public void close() {
        subFilter.close();
    }

    public ISubFilter getSubFilter() {
        return subFilter;
    }
}
