package net.sf.okapi.common.resource;

/**
 * Enums define various matching criteria between two lists of codes (normally source and target).
 */
public enum CodeMatchStrategy {
    /**
     * Use for leveraged targets. We assume Code id's will *not* match.
     * Minimally match on {@link Code#getTagType()}, but try {@link Code#getId} and {@link Code#getOriginalId()} and
     * {@link Code#getData()} first. With special logic for matching isolated codes.
     */
    LAX,
    /**
     * Use as default for filter merging. Must match Code.ID or Code.originalID
     * Try {@link Code#getId} or {@link Code#getOriginalId()} first, if no match then try {@link Code#getData()}
     * and {@link Code#getTagType()}. With special logic for matching isolated codes.
     */
    STRICT
}
