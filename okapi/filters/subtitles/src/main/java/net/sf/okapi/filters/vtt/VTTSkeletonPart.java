package net.sf.okapi.filters.vtt;

import net.sf.okapi.common.skeleton.GenericSkeletonPart;

public class VTTSkeletonPart extends GenericSkeletonPart {
    private boolean isHeader = false;
    private boolean isEmpty = false;

    public static final String PLACEHOLDER = "placeholder";

    public VTTSkeletonPart(String data, boolean isHeader) {
        super(data);
        // hack since empty skeleton parts get discarded
        if (data.isEmpty()) {
            isEmpty = true;
            append(PLACEHOLDER);
        }
        this.isHeader = isHeader;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public boolean isEmpty() {
        return isEmpty;
    }
}
