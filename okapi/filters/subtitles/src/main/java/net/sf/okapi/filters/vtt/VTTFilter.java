package net.sf.okapi.filters.vtt;

import net.sf.okapi.common.*;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.common.skeleton.ISkeletonWriter;
import net.sf.okapi.filters.subtitles.SubtitleFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UsingParameters(VTTParameters.class)
public class VTTFilter extends SubtitleFilter {

	public static final String FILTER_NAME	= "okf_vtt";
	public static final String FILTER_MIME	= MimeTypeMapper.VTT_MIME_TYPE;

	public static final String HEADER_REGEX = "^(.+)\\s+-->\\s+(.+?)($|\\s+)(.*)";
	public static final Pattern HEADER_PATTERN = Pattern.compile(HEADER_REGEX);
	private static final String TERMINAL_REGEX = "([.．。!！?？؟？][^a-zA-Z0-9]*|</ ?v>)$";

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private BufferedReader reader;

	private boolean captionHasEnded = false;
	
	public VTTFilter() {
		super();
		setName(FILTER_NAME);
		setDisplayName("VTT Filter");
		setMimeType(FILTER_MIME);
		setParameters(new VTTParameters());
		setFilterWriter(createFilterWriter());
		
		addConfiguration(new FilterConfiguration(getName(), FILTER_MIME, getClass().getName(),
				"VTT", "VTT Documents", "okf_vtt.fprm", ".vtt;.srt"));
	}

	@Override
	public void setParameters(IParameters params) {
		this.params = (VTTParameters) params;
	}

	@Override
	public VTTParameters getParameters() { return (VTTParameters) params; }

	@Override
	public ISkeletonWriter createSkeletonWriter() {
		return new VTTSkeletonWriter();
	}

	@Override
	public void close() {
		super.close();
		try {
			if (reader != null) {
				reader.close();
				reader = null;
			}
		} catch (IOException e) {
			throw new OkapiIOException("Could not close " + getDocumentName(), e);
		}

		LOGGER.debug("{} has been closed", getDocumentName());
	}

	@Override
	public void open(RawDocument input, boolean generateSkeleton) {
		super.open(input, generateSkeleton);

		reader = getBufferedReader(input.getReader());
	}

	private static BufferedReader getBufferedReader(Reader inputReader) {
		if (inputReader instanceof BufferedReader) {
			return (BufferedReader) inputReader;
		} else {
			return new BufferedReader(inputReader);
		}
	}

	@Override
	public Event next() {
		if (eventBuilder.hasQueuedEvents()) {
			return eventBuilder.next();
		}

		String line = null;
		try {
			while (!isCanceled()) {
				line = reader.readLine();
				if (line == null) {
					break;
				}

				Matcher matcher = HEADER_PATTERN.matcher(line);
				if (matcher.find()) {
					if (!eventBuilder.isInsideTextRun()) {
						eventBuilder.startTextUnit();
					}
					addCaptionAnnotation(matcher.group(1), matcher.group(2));
					appendToSkeleton(line, true);
					captionHasEnded = false;

				} else if (eventBuilder.isInsideTextRun()) {
					if (line.trim().isEmpty()) {
						// Consider empty captions as complete text units
						String captionText = eventBuilder.peekMostRecentTextUnit().getSource().getFirstContent().getText();
						if (captionText.isEmpty() || endsWithPunctuation(captionText)) {
							eventBuilder.endTextUnit();
							addToDocumentPart(line);
							addToDocumentPart(getNewlineType());
						} else {
							captionHasEnded = true;
							appendToSkeleton(line, false);
						}
					} else {
						if (captionHasEnded) {
							appendToSkeleton(line, false);
						} else {
							addToTextUnit(line);
						}
					}
				} else {
					addToDocumentPart(line);
					addToDocumentPart(getNewlineType());
				}

				if (eventBuilder.hasQueuedEvents()) {
					break;
				}
			}
		} catch (IOException e) {
			throw new OkapiIOException("Reading error", e);
		}

		if (line == null) {
			endFilter();
		}

		return eventBuilder.next();
	}

	private void appendToSkeleton(String text, boolean isHeader) {
		eventBuilder.appendToSkeleton(new VTTSkeletonPart(text, isHeader));
	}

	protected String getTerminalRegex() { return TERMINAL_REGEX; }
}
