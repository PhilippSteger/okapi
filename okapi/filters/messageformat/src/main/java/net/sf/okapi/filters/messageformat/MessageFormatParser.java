/*
 * ====================================================================
 *   Copyright (C) 2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.text.MessagePattern;
import com.ibm.icu.text.MessagePatternUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.IcuMessageEncoder;

import java.text.Format;
import java.text.ParseException;
import java.util.*;

public class MessageFormatParser implements AutoCloseable {
    private final IcuMessageEncoder encoder;
    private MessagePatternUtil.MessageNode rootNode;

    public MessageFormatParser() {
        this.encoder = new IcuMessageEncoder();
    }

    public MessageFormatParser(String message) throws Exception {
        this();
        parse(message);
    }

    public MessagePatternUtil.MessageNode parse(String message) throws Exception {
        close();
        String msg = message;
        if (message == null || message.isEmpty()) {
            msg = "";
        }

        MessagePattern pattern = new MessagePattern();
        pattern.parse(new MessagePattern(msg).autoQuoteApostropheDeep());
        rootNode = MessagePatternUtil.buildMessageNode(pattern);
        return rootNode;
    }

    MessagePatternUtil.MessageNode getRootNode() {
        return rootNode;
    }

    @Override
    public void close() throws Exception {
        rootNode = null;
        encoder.reset();
    }

    @Override
    public String toString() {
        if (rootNode == null || rootNode.getContents().isEmpty()) {
            return "";
        }
        var visitor = new PrintVisitor();
        visitor.visit(rootNode);
        return visitor.toString();
    }

    public String toFormatted(LocaleId targetLocale) throws ParseException {
        if (rootNode == null || rootNode.getContents().isEmpty()) {
            return "";
        }

        var visitor = new FormattedVisitor(targetLocale);
        visitor.visit(rootNode);
        return visitor.toString();
    }

    public void addPluralForms(LocaleId srcLocale, LocaleId trgLocale) throws Exception {
        if (rootNode == null || rootNode.getContents().isEmpty()) {
            return;
        }

        var visitor = new PluralVisitor(srcLocale, trgLocale);
        visitor.visit(rootNode);
        parse(visitor.toString());
    }

    public void normalize() throws Exception {
        if (rootNode == null || rootNode.getContents().isEmpty()) {
            return;
        }
        parse(normalize(toString()));
    }

    /**
     * Normalize the current message string. Easier to parse with the MessagePattern vs {@link INodeVisitor}
     * @param msg the message string to normalize
     * @return the normalized message
     */
    public String normalize(String msg) {
        if (msg == null || msg.isEmpty()) {
            return msg;
        }

        final MessagePattern pattern = new MessagePattern(msg).freeze();
        int firstConditionStart = -1;
        final int pCount = pattern.countParts();
        for (int pIndex = 0; pIndex < pCount; ++pIndex) { // Search for the fist decision
            final MessagePattern.Part part = pattern.getPart(pIndex);
            if (part.getType() == MessagePattern.Part.Type.ARG_START) {
                if (part.getArgType() == MessagePattern.ArgType.SELECT
                        || part.getArgType() == MessagePattern.ArgType.CHOICE
                        || part.getArgType() == MessagePattern.ArgType.PLURAL
                        || part.getArgType() == MessagePattern.ArgType.SELECTORDINAL) {
                    firstConditionStart = pIndex;
                    break;
                }
            }
        }
        // No decisions in the pattern, return as is
        if (firstConditionStart == -1) {
            return msg;
        }

        final MessagePattern.Part begin = pattern.getPart(firstConditionStart);
        final int firstConditionEnd = pattern.getLimitPartIndex(firstConditionStart);
        final MessagePattern.Part end = pattern.getPart(pattern.getLimitPartIndex(firstConditionEnd));

        // Found the first decision
        final String prefix = msg.substring(0, begin.getIndex());
        final String postfix = msg.substring(end.getLimit());
        //    prefix {foo,select,one{unu}two{doi}three{trei}} postfix
        // Becomes
        //    {foo,select,one{prefix unu postfix}two{prefix doi postfix}three{prefix trei postfix}}
        // And the internal messages are normalized, recursively
        final StringBuffer result = new StringBuffer();
        int toOutput = begin.getIndex();
        for (int xx = firstConditionStart + 1; xx < firstConditionEnd; ++xx) {
            final MessagePattern.Part subPart = pattern.getPart(xx);
            if (subPart.getType() == MessagePattern.Part.Type.MSG_START) {
                xx = pattern.getLimitPartIndex(xx);
                final MessagePattern.Part msgEnd = pattern.getPart(xx);
                result.append(msg, toOutput, subPart.getLimit());
                toOutput = msgEnd.getIndex();
                String newMessage = prefix + msg.substring(subPart.getLimit(), msgEnd.getIndex()) + postfix;
                result.append(normalize(newMessage));
            }
        }

        // The rest of the message
        result.append(msg, toOutput, end.getLimit());
        return result.toString();
    }
}
