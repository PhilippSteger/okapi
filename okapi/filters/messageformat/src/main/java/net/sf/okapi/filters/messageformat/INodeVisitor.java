package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessagePattern;
import com.ibm.icu.text.MessagePatternUtil;
import net.sf.okapi.common.StringUtil;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.TextFragment;

import java.util.List;

public interface INodeVisitor {
    default void visit(MessagePatternUtil.MessageNode node) {
        for (MessagePatternUtil.MessageContentsNode contentsNode : node.getContents()) {
            visit(contentsNode);
        }
    }

    default void visit(MessagePatternUtil.MessageContentsNode node) {
        switch (node.getType()) {
            case TEXT:
                visit((MessagePatternUtil.TextNode) node);
                break;
            case ARG:
                visit((MessagePatternUtil.ArgNode) node);
                break;
            case REPLACE_NUMBER:
                break;
        }
    }

    void visit(MessagePatternUtil.TextNode node);

    default void visit(MessagePatternUtil.ArgNode node) {
        MessagePatternUtil.ComplexArgStyleNode complexNode = node.getComplexStyle();
        if (complexNode != null) {
            visit(complexNode);
        }
    }

    default void visit(MessagePatternUtil.VariantNode node) {
        visit(node.getMessage());
    }

    default void visit(MessagePatternUtil.ComplexArgStyleNode node) {
        List<MessagePatternUtil.VariantNode> variants = node.getVariants();
        for (MessagePatternUtil.VariantNode variant : variants) {
            visit(variant);
        }
    }
}
