package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessagePattern;
import com.ibm.icu.text.MessagePatternUtil;
import com.ibm.icu.text.PluralRules;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

import java.util.List;

public class PluralVisitor extends PrintVisitor {
    private final LocaleId srcLocale;
    private final LocaleId trgLocale;
    private final PluralDiff cardinalDiff;
    private final PluralDiff ordinalDiff;

    public PluralVisitor(LocaleId srcLocale, LocaleId trgLocale) {
        super();
        this.srcLocale = srcLocale;
        this.trgLocale = trgLocale;
        cardinalDiff = PluralRulesUtil.diffPluralRules(srcLocale, trgLocale, PluralRules.PluralType.CARDINAL);
        ordinalDiff = PluralRulesUtil.diffPluralRules(srcLocale, trgLocale, PluralRules.PluralType.ORDINAL);
    }

    @Override
    public void visit(MessagePatternUtil.ComplexArgStyleNode node) {
        if (node.getArgType() == MessagePattern.ArgType.CHOICE) {
            // FIXME: deprecated message format should we repair with select or plural?
            throw new OkapiBadFilterInputException("CHOICE format is deprecated and is not supported");
        }

        if (node.hasExplicitOffset()) {
            message.append(String.format(", offset:%d", (int) node.getOffset()));
        } else {
            message.append(",");
        }


        PluralDiff diff = null;
        if (node.getArgType() == MessagePattern.ArgType.PLURAL) {
            diff = cardinalDiff;
        } else if (node.getArgType() == MessagePattern.ArgType.SELECTORDINAL) {
            diff =  ordinalDiff;
        }
        if (diff != null && diff.hasRemoved()) {
            List<MessagePatternUtil.VariantNode> variants = node.getVariants();
            for (MessagePatternUtil.VariantNode variant : variants) {
                if (!diff.getRemoved().contains(variant.getSelector())) {
                    visit(variant);
                }
            }
        } else {
            indent++;
            List<MessagePatternUtil.VariantNode> variants = node.getVariants();
            for (MessagePatternUtil.VariantNode variant : variants) {
                visit(variant);
                message.append("\n");
            }
            message.deleteCharAt(message.length() - 1);
            indent--;
        }

        if (diff != null && diff.hasAdded()) {
            addPluralForms(node.getVariants().get(node.getVariants().size() - 1), diff);
        }
    }

    private void addPluralForms(MessagePatternUtil.VariantNode pluralForm, PluralDiff diff) {
        // Add the plural forms
        for (String form : diff.getAdded()) {
            message.append(String.format("%s%s ", indent(), form));
            message.append("{");
            visit(pluralForm.getMessage());
            message.append("}\n");
        }
        message.deleteCharAt(message.length() - 1);
    }
}
