/*===========================================================================
  Copyright (C) 2019 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.*;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.IcuMessageEncoder;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.filterwriter.GenericContent;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.json.Parameters;
import net.sf.okapi.filters.yaml.YamlFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class MessageFormatFilterTest {
    private FileLocation root;
    private MessageFormatFilter filter;
    private final LocaleId locEN = LocaleId.ENGLISH;
    private final LocaleId locPT = LocaleId.PORTUGUESE;
    private GenericContent fmt;
	private IcuMessageEncoder encoder;

	@Before
    public void messageFormatFilterTest() {
        filter = new MessageFormatFilter();
		filter.getParameters().setAddPluralForms(false);
		filter.setTrgLoc(LocaleId.ENGLISH);
		root = FileLocation.fromClass(getClass());
        fmt = new GenericContent();
		encoder = new IcuMessageEncoder();
    }

    @Test
    public void testDefaultInfo() {
        assertNotNull(filter.getParameters());
        assertNotNull(filter.getName());
        List<FilterConfiguration> list = filter.getConfigurations();
        assertNotNull(list);
        assertFalse(list.isEmpty());
    }

    @Test
    public void testStartDocument() {
		filter.open(new RawDocument("Foo bar {gender, select, male {bizz} other {bazz}}", locEN, locEN));
        Event event;
        if (filter.hasNext()) {
            event = filter.next();
            assertSame("First event is not a StartDocument event.", EventType.START_DOCUMENT, event.getEventType());
            StartDocument sd = (StartDocument) event.getResource();
            assertNotNull("No StartDocument", sd);
            assertNotNull("Encoding is null", sd.getEncoding());
            assertNotNull("ID is null", sd.getId());
            assertNotNull("Language is null", sd.getLocale());
            assertNotNull("Linebreak is null", sd.getLineBreak());
            assertNotNull("FilterWriter is null", sd.getFilterWriter());
            assertNotNull("Mime type is null", sd.getMimeType());
        } else {
            fail();
        }
    }

    @Test
    public void testLineBreaks_CR() {
        String snippet = "Line 1\rLine 2\r";
        String result = FilterTestDriver.generateOutput(getEvents(snippet), locEN,
            filter.createSkeletonWriter(), filter.getEncoderManager());
        assertEquals(snippet, result);
    }

    @Test
    public void testineBreaks_CRLF() {
        String snippet = "Line 1\r\nLine 2\r\n";
        String result = FilterTestDriver.generateOutput(getEvents(snippet), locEN,
            filter.createSkeletonWriter(), filter.getEncoderManager());
        assertEquals(snippet, result);
    }

    @Test
    public void testLineBreaks_LF() {
        String snippet = "Line 1\nLine 2\n";
        String result = FilterTestDriver.generateOutput(getEvents(snippet), locEN,
            filter.createSkeletonWriter(), filter.getEncoderManager());
        assertEquals(snippet, result);
    }

    @Test
    public void testEntry() {
        String snippet = "Line 1";
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
        assertNotNull(tu);
        assertEquals("Line 1", tu.getSource().toString());
    }

    @Test
    public void testCode1() {
        String snippet = "Text {0}";
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
        assertNotNull(tu);
        assertEquals(snippet, tu.getSource().toString());
        assertEquals("Text <1/>", fmt.setContent(tu.getSource().getFirstContent()).toString());
    }

    @Test
    public void testCode2 () {
		String snippet = "{1}Text {foobar}";
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
        assertNotNull(tu);
        assertEquals(snippet, tu.getSource().toString());
		assertEquals("<1/>Text <2/>", fmt.setContent(tu.getSource().getFirstContent()).toString());
    }

    @Test
	public void testCode3() {
		String snippet = "Text {count, number}";
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
        assertNotNull(tu);
        assertEquals(snippet, tu.getSource().toString());
		assertEquals("Text <1/>", fmt.setContent(tu.getSource().getFirstContent()).toString());
    }

    @Test
	public void testCode4() {
		String snippet = "Text {startDate, date, medium}";
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
        assertNotNull(tu);
        assertEquals(snippet, tu.getSource().toString());
		assertEquals("Text <1/>", fmt.setContent(tu.getSource().getFirstContent()).toString());
    }

	@Test
	public void testCode5() {
		String snippet = "Text {gender, select, male {him} female {her} other {they}} End";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("him", "her", "they", "Text [#$sg1] End")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getSource().toString());
		}
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 4);
		assertEquals("Text <1/> End", fmt.setContent(tu.getSource().getFirstContent()).toString());
	}

	@Test
	public void testCode6() {
		// From http://userguide.icu-project.org/formatparse/messages
		String snippet = "{gender_of_host, select, " + "female {" + "{num_guests, plural, offset:1 "
				+ "=0 {{host} does not give a party.}" + "=1 {{host} invites {guest} to her party.}"
				+ "=2 {{host} invites {guest} and one other person to her party.}"
				+ "other {{host} invites {guest} and # other people to her party.}}}" + "male {"
				+ "{num_guests, plural, offset:1 " + "=0 {{host} does not give a party.}"
				+ "=1 {{host} invites {guest} to his party.}"
				+ "=2 {{host} invites {guest} and one other person to his party.}"
				+ "other {{host} invites {guest} and # other people to his party.}}}" + "other {"
				+ "{num_guests, plural, offset:1 " + "=0 {{host} does not give a party.}"
				+ "=1 {{host} invites {guest} to their party.}"
				+ "=2 {{host} invites {guest} and one other person to their party.}"
				+ "other {{host} invites {guest} and # other people to their party.}}}}";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("{host} does not give a party.", "{host} invites {guest} to her party.",
				"{host} invites {guest} and one other person to her party.",
				"{host} invites {guest} and # other people to her party.", "[#$sg2]", "{host} does not give a party.",
				"{host} invites {guest} to his party.", "{host} invites {guest} and one other person to his party.",
				"{host} invites {guest} and # other people to his party.", "[#$sg3]", "{host} does not give a party.",
				"{host} invites {guest} to their party.", "{host} invites {guest} and one other person to their party.",
				"{host} invites {guest} and # other people to their party.", "[#$sg4]")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getSource().toString());
		}
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 4);
		assertEquals("<1/> invites <2/> and <3/> other people to her party.",
				fmt.setContent(tu.getSource().getFirstContent()).toString());
	}

	@Test
	public void testMultipleEmbedded() {
		String snippet = "{gender_of_host, select, " + "female {" + "{num_guests, plural, offset:1 "
				+ "=0 {{host} does not give a party.} " + "=1 {{host} invites {guest} to her party.} "
				+ "=2 {{host} invites {guest} and one other person to her party.} "
				+ "other {{host} invites {guest} and # other people to her party.}}} " + "male {"
				+ "{num_guests, plural, offset:1 " + "=0 {{host} does not give a party.} "
				+ "=1 {{host} invites {guest} to his party.} "
				+ "=2 {{host} invites {guest} and one other person to his party.} "
				+ "other {{host} invites {guest} and # other people to his party.}}} " + "other {"
				+ "{num_guests, plural, offset:1 " + "=0 {{host} does not give a party.} "
				+ "=1 {{host} invites {guest} to their party.} "
				+ "=2 {{host} invites {guest} and one other person to their party.} "
				+ "other {{host} invites {guest} and # other people to their party.}}}}";
		String result = FilterTestDriver.generateOutput(getEvents(snippet), locEN,
				filter.createSkeletonWriter(), filter.getEncoderManager());
		assertEquals(snippet, result);
	}

	@Test
	public void testMany1() {
		String snippet = "There {count, plural, =0 {are no items.} =1 {is one item.} other {are # items.}}";
		String result = FilterTestDriver.generateOutput(getEvents(snippet), locEN,
				filter.createSkeletonWriter(), filter.getEncoderManager());
		assertEquals(snippet, result);
	}

	@Test
	public void testGenderNames() {
		String snippet = "Text {gender, select, male {him} female {her} other {they}} {foo} {0} End";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("gender_male", "gender_female", "gender_other")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getName());
		}
	}

	@Test
	public void testPluralNames() {
		String snippet = "Text {count, plural, zero {foo} =1 {bar} =2 {baz} other {bazinga}} {foo} {0} End";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("count_zero", "count_=1", "count_=2", "count_other")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getName());
		}
	}

	@Test
	public void testPluralNames2() {
		String snippet = "You have {numPhotos, plural, =0 {no photos.} =1 {one photo.} other {# photos.}}";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("numPhotos_=0", "numPhotos_=1", "numPhotos_other")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getName());
		}
	}

	@Test
	public void testPluralNames3() {
		String snippet = "{members, plural, \n" +
				"    =0 {Нет доступных членов} \n" +
				"    one {Есть один член.} \n" +
				"    few {Имеется # члена.} \n" +
				"    many {Есть # членов.} \n" +
				"    other {# члена.}}";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("members_=0", "members_one", "members_few", "members_many", "members_other")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getName());
		}
	}

	@Test
	public void testEmbeddedPluralNames() {
		String snippet = "{0, plural,one {You have {1, plural, one {# apple} other {# apples}}} other {You and # others have {1, plural, one {# apple} other {# apples}}}}";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("1_one", "1_other", "0_one", "1_one", "1_other", "0_other")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, tu.getName());
		}
	}

	@Test(expected = OkapiException.class)
	public void testInvalid() {
		String snippet = "Text {gender, select, male {him} female {her} other {they} End}";
		getEvents(snippet);
	}

    @Test
	public void testLiterals() {
		String snippet = "{1}'{foobar}'";
        ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
        assertNotNull(tu);
		String out = fmt.setContent(tu.getSource().getFirstContent()).toString();
		assertEquals("<1/>'{foobar}'", encoder.encode(out, EncoderContext.TEXT));
    }

	@Test
	public void testOneQuote() {
		String snippet = "{1}'{foobar}";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
		assertNotNull(tu);
		String out = fmt.setContent(tu.getSource().getFirstContent()).toString();
		assertEquals("<1/>'{foobar}'", encoder.encode(out, EncoderContext.TEXT));
	}

	@Test
	public void testQuotedQuote() {
		String snippet = "''{1}''{foobar}";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
		assertNotNull(tu);
		String out = fmt.setContent(tu.getSource().getFirstContent()).toString();
		assertEquals("''<1/>''<2/>", encoder.encode(out, EncoderContext.TEXT));
	}

	@Test
	public void testDeepQuotes() {
		String snippet = "I don't '{know}' {gender,select,female{h''er}other{h'im}}.";
		List<Event> events = getEvents(snippet);
		int n = 1;
		for (String expected : Arrays.asList("h''er", "h''im", "I don''t '{know}' [#$sg1].")) {
			ITextUnit tu = FilterTestDriver.getTextUnit(events, n++);
			assertNotNull(tu);
			assertEquals(expected, encoder.encode(tu.getSource().toString(), EncoderContext.TEXT));
		}
	}

	@Test
	public void testMessageFormatSubfilter() {
		String snippet = "{ \"key1\": \"Text {gender, select, male {him} female {her} other {they}} {foo} {0} End\" }";
		IFilter parentFilter = new JSONFilter();
		IFilterConfigurationMapper mapper = new ThreadSafeFilterConfigurationMapper();
		mapper.addConfigurations(MessageFormatFilter.class.getCanonicalName());
		parentFilter.setFilterConfigurationMapper(mapper);
		Parameters params = (Parameters) parentFilter.getParameters();
		params.setSubfilter("okf_messageformat");
		assertEquals(snippet, FilterTestDriver.generateOutput(
				parentFilter, getEvents(snippet, parentFilter), locPT, StandardCharsets.UTF_8));
	}

	@Test
	public void testMessageFormatSubfilterYaml() {
		String snippet = "key1: \n" +
				" Text {gender, select, male {him} female {her} other {they}} {foo} {0} End";
		IFilter parentFilter = new YamlFilter();
		IFilterConfigurationMapper mapper = new ThreadSafeFilterConfigurationMapper();
		mapper.addConfigurations(MessageFormatFilter.class.getCanonicalName());
		parentFilter.setFilterConfigurationMapper(mapper);
		net.sf.okapi.filters.yaml.Parameters params = (net.sf.okapi.filters.yaml.Parameters) parentFilter.getParameters();
		params.setSubfilter("okf_messageformat");
		assertEquals(snippet, FilterTestDriver.generateOutput(
				parentFilter, getEvents(snippet, parentFilter), locPT, StandardCharsets.UTF_8));
	}

	@Test
	public void testDeepEmbeddedSubfilterYaml() {
		String snippet = "key1: \n" +
				" \"{gender, select, male {{num_apples, plural, one {He has {num_oranges, plural, one {an orange} other {# oranges}}} other {He has # apples}}} female {{num_apples, plural, one {She has an apple} other {She has # apples}}} other {{num_apples, plural, one {They have an apple} other {They have # apples}}}}\"";
		IFilter parentFilter = new YamlFilter();
		IFilterConfigurationMapper mapper = new ThreadSafeFilterConfigurationMapper();
		mapper.addConfigurations(MessageFormatFilter.class.getCanonicalName());
		parentFilter.setFilterConfigurationMapper(mapper);
		net.sf.okapi.filters.yaml.Parameters params = (net.sf.okapi.filters.yaml.Parameters) parentFilter.getParameters();
		params.setSubfilter("okf_messageformat");
		assertEquals(snippet, FilterTestDriver.generateOutput(
				parentFilter, getEvents(snippet, parentFilter), locPT, StandardCharsets.UTF_8));
	}

	@Test
	public void testDeepEmbeddedSubfilterJson() {
		String snippet = "{ \"key1\": \n" +
				" \"{gender, select, male {{num_apples, plural, one {He has {num_oranges, plural, one {an orange} other {# oranges}}} other {He has # apples}}} female {{num_apples, plural, one {She has an apple} other {She has # apples}}} other {{num_apples, plural, one {They have an apple} other {They have # apples}}}}\"}";
		IFilter parentFilter = new JSONFilter();
		IFilterConfigurationMapper mapper = new ThreadSafeFilterConfigurationMapper();
		mapper.addConfigurations(MessageFormatFilter.class.getCanonicalName());
		parentFilter.setFilterConfigurationMapper(mapper);
		net.sf.okapi.filters.json.Parameters params = (net.sf.okapi.filters.json.Parameters) parentFilter.getParameters();
		params.setSubfilter("okf_messageformat");
		assertEquals(snippet, FilterTestDriver.generateOutput(
				parentFilter, getEvents(snippet, parentFilter), locPT, StandardCharsets.UTF_8));
	}

	@Test
	public void testOffset() {
		String snippet = "trains_found: >\n" +
				"  {n, plural, offset:2 =0 {No (#) trains are available} one {# train is available} other {# trains are available}}";
		IFilter parentFilter = new YamlFilter();
		IFilterConfigurationMapper mapper = new ThreadSafeFilterConfigurationMapper();
		mapper.addConfigurations(MessageFormatFilter.class.getCanonicalName());
		parentFilter.setFilterConfigurationMapper(mapper);
		net.sf.okapi.filters.yaml.Parameters params = (net.sf.okapi.filters.yaml.Parameters) parentFilter.getParameters();
		params.setSubfilter("okf_messageformat");
		assertEquals(snippet, FilterTestDriver.generateOutput(
				parentFilter, getEvents(snippet, parentFilter), locPT, StandardCharsets.UTF_8));
	}

	@Test
	public void testSelectOrdinal() {
		String snippet = "You are the {0,selectordinal,one{#st} two{#nd} few{#rd} other{#th}} person here.";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet), 1);
		assertNotNull(tu);
		assertEquals("<1/>st", fmt.setContent(tu.getSource().getFirstContent()).toString());

		tu = FilterTestDriver.getTextUnit(getEvents(snippet), 2);
		assertEquals("<1/>nd", fmt.setContent(tu.getSource().getFirstContent()).toString());

		tu = FilterTestDriver.getTextUnit(getEvents(snippet), 3);
		assertEquals("<1/>rd", fmt.setContent(tu.getSource().getFirstContent()).toString());

		tu = FilterTestDriver.getTextUnit(getEvents(snippet), 4);
		assertEquals("<1/>th", fmt.setContent(tu.getSource().getFirstContent()).toString());
	}

	@Test
	public void testInlineCodes() {
		String snippet = "{count, plural, =0 {<b>No trains are available</b>} one {<i> {count} train is available</i>} other {<u> {count} trains are available</u>}}";
		net.sf.okapi.filters.messageformat.Parameters params = filter.getParameters();
		params.setUseCodeFinder(true);

		List<Event> events = FilterTestDriver.getEvents(filter, snippet, params, locEN, locPT);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("<1/>No trains are available<2/>", fmt.setContent(tu.getSource().getFirstContent()).toString());

		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("<1/> <2/> train is available<3/>", fmt.setContent(tu.getSource().getFirstContent()).toString());

		tu = FilterTestDriver.getTextUnit(events, 3);
		assertNotNull(tu);
		assertEquals("<1/> <2/> trains are available<3/>", fmt.setContent(tu.getSource().getFirstContent()).toString());

		assertEquals(snippet, FilterTestDriver.generateOutput(filter, events, locPT, StandardCharsets.UTF_8));
	}

	private ArrayList<Event> getEvents(String snippet, IFilter filter, LocaleId trgLoc) {
		return FilterTestDriver.getEvents(filter, snippet, locEN, trgLoc);
	}

	private ArrayList<Event> getEvents(String snippet, IFilter filter) {
		return FilterTestDriver.getEvents(filter, snippet, locEN);
	}

    private ArrayList<Event> getEvents(String snippet, LocaleId trgLoc) {
        return FilterTestDriver.getEvents(filter, snippet, locEN, trgLoc);
    }

	private ArrayList<Event> getEvents(String snippet) {
		return FilterTestDriver.getEvents(filter, snippet, locEN);
	}
}
