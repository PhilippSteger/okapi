/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface SourceAndTargetColumns {
    Set<String> source();
    String targetFor(final String source);

    class Default implements SourceAndTargetColumns {
        final Map<String, String> targetBySourceColumns;

        Default(final List<String> sourceColumns, final List<String> targetColumns) {
            this.targetBySourceColumns = IntStream.range(0, Math.min(sourceColumns.size(), targetColumns.size()))
                .boxed()
                .collect(Collectors.toMap(sourceColumns::get, targetColumns::get));
        }

        @Override
        public Set<String> source() {
            return this.targetBySourceColumns.keySet();
        }

        @Override
        public String targetFor(final String source) {
            return this.targetBySourceColumns.get(source);
        }
    }
}
