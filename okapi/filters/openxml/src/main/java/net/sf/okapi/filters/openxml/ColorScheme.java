/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

interface ColorScheme {
    String NAME = "clrScheme";

    String colorValueFor(final int index);
    String colorValueFor(final String name);

    void readWith(final XMLEventReader reader) throws XMLStreamException;

    Markup asMarkup();

    final class Empty implements ColorScheme {
        private static final String EMPTY = "";
        @Override
        public String colorValueFor(final int index) {
            return EMPTY;
        }

        @Override
        public String colorValueFor(final String name) {
            return EMPTY;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements ColorScheme {
        private static final String EXTENSION_LIST = "extLst";
        private final PresetColorValues presetColorValues;
        private final StartElement startElement;
        private final List<NamedColor> namedColors;
        private List<XMLEvent> extensionListEvents;
        private EndElement endElement;

        Default(
            final PresetColorValues presetColorValues,
            final StartElement startElement
        ) {
            this(presetColorValues, startElement, new ArrayList<>());
        }

        Default(
            final PresetColorValues presetColorValues,
            final StartElement startElement,
            final List<NamedColor> namedColors
        ) {
            this.presetColorValues = presetColorValues;
            this.startElement = startElement;
            this.namedColors = namedColors;
        }

        @Override
        public String colorValueFor(final int index) {
            return this.namedColors.get(index).color().value().asRgb();
        }

        @Override
        public String colorValueFor(final String name) {
            final List<NamedColor> ncs = this.namedColors.stream()
                .filter(nc -> nc.name().equals(name))
                .collect(Collectors.toList());
            if (1 != ncs.size()) {
                throw new IllegalStateException("Unsupported number of named colors found: ".concat(String.valueOf(ncs.size())));
            }
            return ncs.get(0).color().value().asRgb();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (e.isStartElement()) {
                    final StartElement se = e.asStartElement();
                    if (NamedColor.NAMES.contains(se.getName().getLocalPart())) {
                        final NamedColor nc = new NamedColor.Default(
                            this.presetColorValues,
                            this,
                            se
                        );
                        nc.readWith(reader);
                        this.namedColors.add(nc);
                        continue;
                    }
                    if (EXTENSION_LIST.equals(se.getName().getLocalPart())) {
                        this.extensionListEvents = XMLEventHelpers.eventsFor(se, reader);
                    }
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final int initialMarkupComponentsCapacity = 2 + this.namedColors.size();
            final int initialEventsCapacity = null == this.extensionListEvents
                ? 2
                : 1 + this.extensionListEvents.size();
            final MarkupBuilder mb = new MarkupBuilder(
                new Markup.General(
                    new ArrayList<>(initialMarkupComponentsCapacity)
                ),
                new ArrayList<>(initialEventsCapacity)
            );
            mb.add(this.startElement);
            if (null != this.extensionListEvents) {
                mb.addAll(this.extensionListEvents);
            }
            this.namedColors.forEach(nc -> mb.add(nc.asMarkup()));
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
