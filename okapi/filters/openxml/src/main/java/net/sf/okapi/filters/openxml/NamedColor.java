 /*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

 import javax.xml.stream.XMLEventReader;
 import javax.xml.stream.XMLStreamException;
 import javax.xml.stream.events.EndElement;
 import javax.xml.stream.events.StartElement;
 import javax.xml.stream.events.XMLEvent;
 import java.util.ArrayList;
 import java.util.Set;

interface NamedColor {
    Set<String> NAMES = Set.of(
        "dk1", "lt1", "dk2", "lt2",
        "accent1", "accent2", "accent3", "accent4", "accent5", "accent6",
        "hlink", "folHlink"
    );
    String name();
    Color color();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Default implements NamedColor {
        private final PresetColorValues presetColorValues;
        private final ColorScheme colorScheme;
        private final StartElement startElement;
        private Color color;
        private EndElement endElement;

        Default(
            final PresetColorValues presetColorValues,
            final ColorScheme colorScheme,
            final StartElement startElement
        ) {
            this.presetColorValues = presetColorValues;
            this.colorScheme = colorScheme;
            this.startElement = startElement;
        }

        @Override
        public String name() {
            return this.startElement.getName().getLocalPart();
        }

        @Override
        public Color color() {
            return this.color;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                switch (se.getName().getLocalPart()) {
                    case Color.Default.NAME:
                        this.color = new Color.Default(this.presetColorValues, se);
                        this.color.readWith(reader);
                        continue;
                    case Color.PercentageRgb.NAME:
                        this.color = new Color.PercentageRgb(this.presetColorValues, new Color.Default(this.presetColorValues, se));
                        this.color.readWith(reader);
                        continue;
                    case Color.Hsl.NAME:
                        this.color = new Color.Hsl(this.presetColorValues, new Color.Default(this.presetColorValues, se));
                        this.color.readWith(reader);
                        continue;
                    case Color.System.NAME:
                        this.color = new Color.System(new Color.Default(this.presetColorValues, se));
                        this.color.readWith(reader);
                        continue;
                    case Color.Scheme.NAME:
                        this.color = new Color.Scheme(this.presetColorValues, this.colorScheme, new Color.Default(this.presetColorValues, se));
                        this.color.readWith(reader);
                        continue;
                    case Color.Preset.NAME:
                        this.color = new Color.Preset(this.presetColorValues, new Color.Default(this.presetColorValues, se));
                        this.color.readWith(reader);
                    default:
                        throw new IllegalStateException("Unsupported color: ".concat(se.getName().getLocalPart()));
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(
                new Markup.General(
                    new ArrayList<>(3)
                ),
                new ArrayList<>(1)
            );
            mb.add(this.startElement);
            mb.add(this.color.asMarkup());
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
