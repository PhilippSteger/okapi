/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;

interface PatternFill {
    String NAME = "patternFill";
    Color backgroundColor();
    Color foregroundColor();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements PatternFill {
        @Override
        public Color backgroundColor() {
            return new Color.Empty();
        }

        @Override
        public Color foregroundColor() {
            return new Color.Empty();
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements PatternFill {
        private final PresetColorValues presetColorValues;
        private final SystemColorValues systemColorValues;
        private final IndexedColors indexedColors;
        private final Theme theme;
        private final StartElement startElement;
        private Color backgroundColor;
        private Color foregroundColor;
        private EndElement endElement;

        Default(
            final PresetColorValues presetColorValues,
            final SystemColorValues systemColorValues,
            final IndexedColors indexedColors,
            final Theme theme,
            final StartElement startElement
        ) {
            this.presetColorValues = presetColorValues;
            this.systemColorValues = systemColorValues;
            this.indexedColors = indexedColors;
            this.theme = theme;
            this.startElement = startElement;
        }

        @Override
        public Color backgroundColor() {
            if (null == this.backgroundColor) {
                this.backgroundColor = new Color.Empty();
            }
            return this.backgroundColor;
        }

        @Override
        public Color foregroundColor() {
            if (null == this.foregroundColor) {
                this.foregroundColor = new Color.Empty();
            }
            return this.foregroundColor;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (Color.Argb.BACKGROUND_NAME.equals(se.getName().getLocalPart())) {
                    this.backgroundColor = new Color.Argb(
                        this.presetColorValues,
                        this.systemColorValues,
                        this.indexedColors,
                        this.theme,
                        new Color.Default(this.presetColorValues, se)
                    );
                    this.backgroundColor.readWith(reader);
                    continue;
                }
                if (Color.Argb.FOREGROUND_NAME.equals(se.getName().getLocalPart())) {
                    this.foregroundColor = new Color.Argb(
                        this.presetColorValues,
                        this.systemColorValues,
                        this.indexedColors,
                        this.theme,
                        new Color.Default(this.presetColorValues, se)
                    );
                    this.foregroundColor.readWith(reader);
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(
                new Markup.General(
                    new ArrayList<>(4)
                ),
                new ArrayList<>(1)
            );
            mb.add(this.startElement);
            mb.add(foregroundColor().asMarkup());
            mb.add(backgroundColor().asMarkup());
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
