/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.LocaleId;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

interface ContentCategoriesDetection {
    void performFor(final RunFonts runFonts, final String runText);

    final class NonApplicable implements ContentCategoriesDetection {
        @Override
        public void performFor(final RunFonts runFonts, final String runText) {
        }
    }

    final class Default implements ContentCategoriesDetection {
        private static final Pattern ASCII_CHARACTERS = Pattern.compile(".*[\u0000-\u007F].*");

        /**
         * All latin character ranges according to
         * <a href="https://blogs.msdn.microsoft.com/officeinteroperability/2013/04/22/office-open-xml-themes-schemes-and-fonts/">
         * Office Open XML Themes, Schemes and Fonts</a>.
         */
        private static final Pattern LATIN_CHARACTERS = Pattern.compile(
            ".*[\u0080-\u00A6\u00A9-\u00AF\u00B2-\u00B3\u00B5-\u00D6\u00D8-\u00F6"
                + "\u00F8-\u058F\u10A0-\u10FF\u1200-\u137F\u13A0-\u177F\u1D00-\u1D7F\u1E00-\u1FFF"
                + "\u2000-\u200B\u2010-\u2017\u201F-\u2029\u2030-\u2046\u204A-\u245F\u27C0-\u2BFF\uD835"
                + "\uFB00-\uFB17\uFE50-\uFE6F].*"
        );

        /**
         * Symbols according to
         * <a href="https://blogs.msdn.microsoft.com/officeinteroperability/2013/04/22/office-open-xml-themes-schemes-and-fonts/">
         * Office Open XML Themes, Schemes and Fonts</a>.
         */
        private static final Pattern SYMBOLS = Pattern.compile(".*[\uF000-\uF0FF].*");

        /**
         * In dependence of the {@code w:lang} property, the characters selected by this selector may be
         * considered as latin or east asian characters. Further information can be found under
         * <a href="https://blogs.msdn.microsoft.com/officeinteroperability/2013/04/22/office-open-xml-themes-schemes-and-fonts/">
         * Office Open XML Themes, Schemes and Fonts</a>.
         */
        private static final Pattern SHARED_CHARACTERS = Pattern.compile(".*[\u2018-\u201E].*");

        /**
         * All complex script character ranges according to
         * <a href="https://blogs.msdn.microsoft.com/officeinteroperability/2013/04/22/office-open-xml-themes-schemes-and-fonts/">
         * Office Open XML Themes, Schemes and Fonts</a>.
         */
        private static final Pattern COMPLEX_SCRIPT_CHARACTERS = Pattern.compile(
            ".*[\u0590-\u074F\u0780-\u07BF\u0900-\u109F\u1780-\u18AF"
                + "\u200C-\u200F\u202A-\u202F\u2670-\u2671\uFB1D-\uFB4F].*"
        );

        /**
         * Simple east asia character selector according to
         * <a href="https://blogs.msdn.microsoft.com/officeinteroperability/2013/04/22/office-open-xml-themes-schemes-and-fonts/">
         * Office Open XML Themes, Schemes and Fonts</a>.
         * <p>
         * Note that also any character not matching a certain selector may be considered as east asia,
         * too.
         */
        private static final Pattern EAST_ASIAN_CHARACTERS = Pattern.compile(".*[\u3099-\u309A].*");

        /**
         * Contains the {@link LocaleId locales} of the east asian languages that influence the
         * effective content categories for
         * {@link #SHARED_CHARACTERS quotation characters}.
         */
        private static final Set<LocaleId> EAST_ASIAN_LOCALES = new HashSet<>(
            Arrays.asList(
                LocaleId.fromBCP47("ii-CN"),
                LocaleId.fromBCP47("ja-JP"),
                LocaleId.fromBCP47("ko-KR"),
                LocaleId.fromBCP47("zh-CN"),
                LocaleId.fromBCP47("zh-HK"),
                LocaleId.fromBCP47("zh-MO"),
                LocaleId.fromBCP47("zh-SG"),
                LocaleId.fromBCP47("zh-TW")
            )
        );

        private final LocaleId locale;

        Default(final LocaleId locale) {
            this.locale = locale;
        }

        @Override
        public void performFor(final RunFonts runFonts, final String runText) {
            if (!runText.isEmpty()) {
                if (ASCII_CHARACTERS.matcher(runText).matches()) {
                    runFonts.addDetected(
                        runFonts.getContentCategory(RunFonts.ContentCategory.ASCII_THEME, RunFonts.ContentCategory.ASCII)
                    );
                }
                if (SHARED_CHARACTERS.matcher(runText).matches()) {
                    if (EAST_ASIAN_LOCALES.contains(this.locale)) {
                        runFonts.addDetected(
                            runFonts.getContentCategory(RunFonts.ContentCategory.EAST_ASIAN_THEME, RunFonts.ContentCategory.EAST_ASIAN)
                        );
                    } else {
                        runFonts.addDetected(
                            runFonts.getContentCategory(RunFonts.ContentCategory.HIGH_ANSI_THEME, RunFonts.ContentCategory.HIGH_ANSI)
                        );
                    }
                }
                if (LATIN_CHARACTERS.matcher(runText).matches() || SYMBOLS.matcher(runText).matches()) {
                    runFonts.addDetected(
                        runFonts.getContentCategory(RunFonts.ContentCategory.HIGH_ANSI_THEME, RunFonts.ContentCategory.HIGH_ANSI)
                    );
                }
                if (COMPLEX_SCRIPT_CHARACTERS.matcher(runText).matches()) {
                    runFonts.addDetected(
                        runFonts.getContentCategory(RunFonts.ContentCategory.COMPLEX_SCRIPT_THEME, RunFonts.ContentCategory.COMPLEX_SCRIPT)
                    );
                }
                if (EAST_ASIAN_CHARACTERS.matcher(runText).matches() || matchesOtherCharacters(runText)) {
                    runFonts.addDetected(
                        runFonts.getContentCategory(RunFonts.ContentCategory.EAST_ASIAN_THEME, RunFonts.ContentCategory.EAST_ASIAN)
                    );
                }
            }
        }

        private boolean matchesOtherCharacters(final String text) {
            return !ASCII_CHARACTERS.matcher(text).matches()
                && !LATIN_CHARACTERS.matcher(text).matches()
                && !SYMBOLS.matcher(text).matches()
                && !SHARED_CHARACTERS.matcher(text).matches()
                && !COMPLEX_SCRIPT_CHARACTERS.matcher(text).matches()
                && !EAST_ASIAN_CHARACTERS.matcher(text).matches();
        }
    }
}
