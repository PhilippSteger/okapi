/*
 * =============================================================================
 * Copyright (C) 2010-2020 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.ListIterator;

/**
 * Provides a markup clarification.
 */
class MarkupClarification {
    private final MarkupComponentClarification sheetViewClarification;
    private final MarkupComponentClarification alignmentClarification;
    private final MarkupComponentClarification nameTranslationClarification;
    private final MarkupComponentClarification sheetTranslationClarification;
    private final MarkupComponentClarification formulaTranslationClarification;
    private final MarkupComponentClarification presentationClarification;
    private final BlockPropertiesClarification tablePropertiesClarification;
    private final BlockPropertiesClarification textBodyPropertiesClarification;
    private final BlockPropertiesClarification paragraphPropertiesClarification;
    private final StylesClarification powerpointStylesClarification;
    private final StylesClarification wordStylesClarification;

    MarkupClarification(final MarkupClarificationConfiguration configuration) {
        this(
            configuration.sheetViewClarification(),
            configuration.alignmentClarification(),
            configuration.nameTranslationClarification(),
            configuration.sheetTranslationClarification(),
            configuration.formulaTranslationClarification(),
            configuration.presentationClarification(),
            configuration.tablePropertiesClarification(),
            configuration.textBodyPropertiesClarification(),
            configuration.paragraphPropertiesClarification(),
            configuration.powerpointStyleClarification(),
            configuration.wordStylesClarification()
        );
    }

    /**
     * Constructs the markup clarification.
     * @param sheetViewClarification The sheet view clarification
     * @param alignmentClarification The alignment clarification
     * @param nameTranslationClarification The name translation clarification
     * @param sheetTranslationClarification The sheet name translation clarification
     * @param formulaTranslationClarification The formula translation clarification
     * @param presentationClarification The presentation clarification
     * @param tablePropertiesClarification The table properties clarification
     * @param textBodyPropertiesClarification The text body properties clarification
     * @param paragraphPropertiesClarification The paragraph properties clarification
     * @param powerpointStylesClarification The powerpoint styles clalrification
     * @param wordStylesClarification The word styles clarification
     */
    MarkupClarification(
        final MarkupComponentClarification sheetViewClarification,
        final MarkupComponentClarification alignmentClarification,
        final MarkupComponentClarification nameTranslationClarification,
        final MarkupComponentClarification sheetTranslationClarification,
        final MarkupComponentClarification formulaTranslationClarification,
        final MarkupComponentClarification presentationClarification,
        final BlockPropertiesClarification tablePropertiesClarification,
        final BlockPropertiesClarification textBodyPropertiesClarification,
        final BlockPropertiesClarification paragraphPropertiesClarification,
        final StylesClarification powerpointStylesClarification,
        final StylesClarification wordStylesClarification
    ) {
        this.sheetViewClarification = sheetViewClarification;
        this.alignmentClarification = alignmentClarification;
        this.nameTranslationClarification = nameTranslationClarification;
        this.sheetTranslationClarification = sheetTranslationClarification;
        this.formulaTranslationClarification = formulaTranslationClarification;
        this.presentationClarification = presentationClarification;
        this.tablePropertiesClarification = tablePropertiesClarification;
        this.textBodyPropertiesClarification = textBodyPropertiesClarification;
        this.paragraphPropertiesClarification = paragraphPropertiesClarification;
        this.powerpointStylesClarification = powerpointStylesClarification;
        this.wordStylesClarification = wordStylesClarification;
    }

    /**
     * Performs for markup.
     */
    void performFor(Markup markup) {
        ListIterator<MarkupComponent> iterator = markup.components().listIterator();

        while (iterator.hasNext()) {
            final MarkupComponent component = iterator.next();
            if (MarkupComponent.isSheetViewStart(component)) {
                this.sheetViewClarification.performFor(component);
            } else if (MarkupComponent.isAlignmentStart(component)) {
                this.alignmentClarification.performFor(component);
            } else if (MarkupComponent.isTableColumnStart(component)
                || MarkupComponent.isDataFieldStart(component)
                || MarkupComponent.isCacheFieldStart(component)) {
                this.nameTranslationClarification.performFor(component);
            } else if (MarkupComponent.isWorksheetSourceStart(component)) {
                this.sheetTranslationClarification.performFor(component);
            } else if (MarkupComponent.isFormula(component)) {
                this.formulaTranslationClarification.performFor(component);
            } else if (MarkupComponent.isPresentationStart(component)) {
                this.presentationClarification.performFor(component);
            } else if (MarkupComponent.isTableStart(component)) {
                this.tablePropertiesClarification.performWith(iterator);
            } else if (MarkupComponent.isTextBodyStart(component)) {
                this.textBodyPropertiesClarification.performWith(iterator);
            } else if (MarkupComponent.isPowerpointStylesStart(component)) {
                this.powerpointStylesClarification.performWith(iterator);
            } else if (MarkupComponent.isStyledStart(component)) {
                this.paragraphPropertiesClarification.performWith(iterator);
            } else if (MarkupComponent.isWordStylesStart(component)) {
                this.wordStylesClarification.performWith(iterator);
            }
        }
    }
}
