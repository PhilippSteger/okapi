/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.time.LocalDateTime;

interface ExcelNumber {
    String formattedWith(final NumberFormat numberFormat);

    class Default implements ExcelNumber {
        private final String value;
        private final boolean date1904;

        Default(final String value, final boolean date1904) {
            this.value = value;
            this.date1904 = date1904;
        }

        @Override
        public String formattedWith(final NumberFormat numberFormat) {
            if (numberFormat.dateTimeBased()) {
                final LocalDateTime ldt = this.date1904
                    ? new SerialDateTime.Epoch1904Based(this.value).asLocalDateTime()
                    : new SerialDateTime.Epoch1900Based(this.value).asLocalDateTime();
                return numberFormat.dateTimeFormatter().format(ldt);
            }
            // todo: provide and apply formatting for numbers
            return this.value;
        }
    }
}
