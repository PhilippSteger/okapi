/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.XMLEventsReader;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

interface Worksheet {
	void readWith(final XMLEventReader reader) throws XMLStreamException;
	Markup asMarkup();

	final class Default implements Worksheet {
		private static final String SHEET_VIEW = "sheetView";
		private static final String SHEET_DATA = "sheetData";
		private static final String ROW = "row";
		private static final String SHEET_CALC_PR = "sheetCalcPr";
		private final ConditionalParameters conditionalParameters;
		private final XMLEventFactory eventFactory;
		private final boolean date1904;
		private final Cells cells;
		private final StyleDefinitions styleDefinitions;
		private final String name;
		private final WorksheetFragments fragments;
		private MarkupBuilder markupBuilder;

		Default(
			final ConditionalParameters conditionalParameters,
			final XMLEventFactory eventFactory,
			final boolean date1904,
			final Cells cells,
			final StyleDefinitions styleDefinitions,
			final String name,
			final WorksheetFragments fragments
		) {
			this.conditionalParameters = conditionalParameters;
			this.eventFactory = eventFactory;
			this.date1904 = date1904;
			this.cells = cells;
			this.styleDefinitions = styleDefinitions;
			this.name = name;
			this.fragments = fragments;
		}

		@Override
		public void readWith(final XMLEventReader reader) throws XMLStreamException {
			this.fragments.readWith(reader);
			final Set<Integer> initialExcludedRows = this.conditionalParameters.worksheetConfigurations().excludedRowsFor(this.name);
			final Set<Integer> metadataRows = this.conditionalParameters.worksheetConfigurations().metadataRowsFor(this.name);
			final Set<Integer> excludedRows = new HashSet<>(
				initialExcludedRows.size() + metadataRows.size()
			);
			excludedRows.addAll(initialExcludedRows);
			excludedRows.addAll(this.fragments.hiddenRows());
			excludedRows.addAll(metadataRows);
			final SourceAndTargetColumns sourceAndTargetColumns = this.conditionalParameters.worksheetConfigurations().sourceAndTargetColumnsFor(this.name);
			final Set<String> initialExcludedColumns = this.conditionalParameters.worksheetConfigurations().excludedColumnsFor(this.name);
			final Set<String> metadataColumns = this.conditionalParameters.worksheetConfigurations().metadataColumnsFor(this.name);
			final Set<String> excludedColumns = new HashSet<>(
				sourceAndTargetColumns.source().size() + initialExcludedColumns.size() + this.fragments.hiddenColumns().size() + metadataColumns.size()
			);
			excludedColumns.addAll(sourceAndTargetColumns.source());
			excludedColumns.addAll(initialExcludedColumns);
			excludedColumns.addAll(this.fragments.hiddenColumns());
			excludedColumns.addAll(metadataColumns);
			this.markupBuilder = new MarkupBuilder(new Markup.General(new ArrayList<>()));
			final XMLEventReader eventsReader = new XMLEventsReader(this.fragments.events());
			boolean inSheetData = false;
			Cells cellsInRow = new Cells.Sorted(
				this.conditionalParameters.getTranslateExcelCellsCopied(),
				sourceAndTargetColumns
			);
			boolean columnsWithStringFunctionValuesPresent = false;

			while (eventsReader.hasNext()) {
				final XMLEvent e = eventsReader.nextEvent();
				if (e.isStartElement()) {
					final StartElement se = e.asStartElement();
					if (SHEET_VIEW.equals(se.getName().getLocalPart())) {
						this.markupBuilder.add(new MarkupComponent.Start(this.eventFactory, se));
						continue;
					} else if (SHEET_DATA.equals(se.getName().getLocalPart())) {
						inSheetData = true;
					} else if (Cell.NAME.equals(se.getName().getLocalPart())) {
						final Cell cell = new Cell.Default(
							this.conditionalParameters,
							this.eventFactory,
							this.date1904,
							this.styleDefinitions,
							excludedRows,
							excludedColumns,
							metadataRows,
							metadataColumns,
							this.fragments.cellReferencesRanges(),
							this.name,
							se
						);
						cell.readWith(eventsReader);
						cellsInRow.add(cell);
						if (!columnsWithStringFunctionValuesPresent && CellType.STRING == cell.type()) {
							columnsWithStringFunctionValuesPresent = true;
						}
						continue;
					} else if (SHEET_CALC_PR.equals(se.getName().getLocalPart())) {
						continue; // skip it
					}
				} else if (e.isEndElement()) {
					final EndElement ee = e.asEndElement();
					if (SHEET_VIEW.equals(ee.getName().getLocalPart())) {
						this.markupBuilder.add(new MarkupComponent.End(ee));
						continue;
					} else if (SHEET_DATA.equals(ee.getName().getLocalPart())) {
						this.markupBuilder.add(ee);
						if (columnsWithStringFunctionValuesPresent) {
							this.markupBuilder.addAll(
								sheetCalcPrEventsAfter(ee)
							);
						}
						inSheetData = false;
						continue;
					} else if (inSheetData && ROW.equals(ee.getName().getLocalPart())) {
						this.cells.add(cellsInRow);
						this.markupBuilder.add(cellsInRow.asMarkup());
						cellsInRow = new Cells.Sorted(
							this.conditionalParameters.getTranslateExcelCellsCopied(),
							sourceAndTargetColumns
						);
					} else if (SHEET_CALC_PR.equals(ee.getName().getLocalPart())) {
						continue; // skip it
					}
				} else if (e.isCharacters() && inSheetData) {
					continue; // skip
				}
				this.markupBuilder.add(e);
			}
		}

		private List<XMLEvent> sheetCalcPrEventsAfter(final EndElement endElement) {
			final List<Attribute> attributes = Collections.singletonList(
				this.eventFactory.createAttribute(
					new QName("fullCalcOnLoad"),
					"true"
				)
			);
			return Arrays.asList(
				this.eventFactory.createStartElement(
					endElement.getName().getPrefix(),
					endElement.getName().getNamespaceURI(),
					SHEET_CALC_PR,
					attributes.iterator(),
					endElement.getNamespaces()
				),
				this.eventFactory.createEndElement(
					endElement.getName().getPrefix(),
					endElement.getName().getNamespaceURI(),
					SHEET_CALC_PR,
					endElement.getNamespaces()
				)
			);
		}

		@Override
		public Markup asMarkup() {
			return this.markupBuilder.build();
		}
	}
}
