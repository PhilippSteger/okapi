/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Objects;

final class CrossSheetCellReference {
    static final String DELIMITER = "!";
    private static final CellReference EMPTY = new CellReference("");
    private final String string;
    private String worksheetName;
    private CellReference cellReference;
    private boolean split;

    CrossSheetCellReference(final String worksheetName, final CellReference cellReference) {
        this(worksheetName +  DELIMITER + cellReference.toString());
        this.worksheetName = worksheetName;
        this.cellReference = cellReference;
        this.split = true;
    }

    CrossSheetCellReference(final String string) {
        this.string = string;
    }

    String worksheetName() {
        if (!this.split) {
            split();
        }
        return this.worksheetName;
    }

    CellReference cellReference() {
        if (!this.split) {
            split();
        }
        return this.cellReference;
    }

    private void split() {
        var idx = this.string.indexOf(DELIMITER);
        if (idx == -1) {
            this.worksheetName = this.string;
            this.cellReference = EMPTY;
        } else {
            this.worksheetName = this.string.substring(0, idx);
            this.cellReference = new CellReference(this.string.substring(idx+1));
        }
        this.split = true;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final CrossSheetCellReference that = (CrossSheetCellReference) o;
        return this.string.equals(that.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.string);
    }

    @Override
    public String toString() {
        return this.string;
    }
}
