/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.LinkedList;
import java.util.List;

interface NumberingLevel {
    String LVL_NAME = "lvl";
    String LVL_OVERRIDE_NAME = "lvlOverride";

    String id();
    List<XMLEvent> eventsBeforeText();
    boolean numberingTextPresent();
    NumberingText numberingText();
    List<XMLEvent> eventsAfterText();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    List<XMLEvent> asEvents();

    final class Default implements NumberingLevel {
        private static final String ILVL = "ilvl";
        private static final String LVL_TEXT = "lvlText";
        private final StartElement startElement;
        private final List<XMLEvent> eventsBeforeText;
        private final List<XMLEvent> eventsAfterText;
        private String id;
        private NumberingText numberingText;
        private boolean referenced;

        Default(final StartElement startElement) {
            this(
                startElement,
                new LinkedList<>(),
                new LinkedList<>()
            );
        }

        Default(
            final StartElement startElement,
            final List<XMLEvent> eventsBeforeText,
            final List<XMLEvent> eventsAfterText
        ) {
            this.startElement = startElement;
            this.eventsBeforeText = eventsBeforeText;
            this.eventsAfterText = eventsAfterText;
        }

        @Override
        public String id() {
            if (null == id) {
                this.id = this.startElement.getAttributeByName(
                    new QName(
                        this.startElement.getName().getNamespaceURI(),
                        Default.ILVL,
                        this.startElement.getName().getPrefix()
                    )
                ).getValue();
            }
            return this.id;
        }

        @Override
        public List<XMLEvent> eventsBeforeText() {
            return this.eventsBeforeText;
        }

        @Override
        public boolean numberingTextPresent() {
            return null != this.numberingText;
        }

        @Override
        public NumberingText numberingText() {
            return this.numberingText;
        }

        @Override
        public List<XMLEvent> eventsAfterText() {
            return this.eventsAfterText;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            boolean beforeText = true;
            this.eventsBeforeText.add(this.startElement);
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isStartElement()) {
                    final StartElement el = e.asStartElement();
                    if (Default.LVL_TEXT.equals(el.getName().getLocalPart())) {
                        this.numberingText = new NumberingText.Default(el);
                        this.numberingText.readWith(reader);
                        beforeText = false;
                    } else if (beforeText) {
                        this.eventsBeforeText.add(e);
                    } else {
                        this.eventsAfterText.add(e);
                    }
                } else if (e.isEndElement()) {
                    final EndElement el = e.asEndElement();
                    if (el.getName().equals(this.startElement.getName())) {
                        this.eventsAfterText.add(e);
                        break;
                    } else if (beforeText) {
                        this.eventsBeforeText.add(e);
                    } else {
                        this.eventsAfterText.add(e);
                    }
                }
            }
        }

        @Override
        public List<XMLEvent> asEvents() {
            final List<XMLEvent> events = new LinkedList<>();
            events.addAll(this.eventsBeforeText);
            if (numberingTextPresent()) {
                events.add(this.numberingText.startElement());
                events.add(this.numberingText.endElement());
            }
            events.addAll(this.eventsAfterText);
            return events;
        }
    }
}
