package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.events.Attribute;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.LocaleId;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestRelationships {
	private final XMLInputFactory inputFactory;
	private final XMLOutputFactory outputFactory;
	private final XMLEventFactory eventFactory;
	private final PartPath basePath;
	private final FileLocation root;

	public TestRelationships() {
		this.inputFactory = XMLInputFactory.newInstance();
		this.outputFactory = XMLOutputFactory.newInstance();
		this.eventFactory = XMLEventFactory.newInstance();
		this.basePath = new PartPath.Default("/ppt");
		this.root = FileLocation.fromClass(getClass());
	}

	@Test
	public void testBasicRels() throws Exception {
		final Relationships relationships = new Relationships.Default(
			this.eventFactory,
			new RelationshipIdGeneration.Default(),
			this.basePath
		);
		try (final Reader reader = getReader("/presentation.xml.rels")) {
			relationships.readWith(this.inputFactory.createXMLEventReader(reader));
		}
		checkRel(relationships, "/ppt/presProps.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/presProps", "rId2");
		checkRel(relationships, "/ppt/theme/theme2.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme", "rId1");
		checkRel(relationships, "/ppt/slideMasters/slideMaster1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster", "rId4");
		checkRel(relationships, "/ppt/tableStyles.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableStyles", "rId3");
		checkRel(relationships, "/ppt/slides/slide4.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide", "rId9");
		checkRel(relationships, "/ppt/slides/slide1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide", "rId6");
		checkRel(relationships, "/ppt/notesMasters/notesMaster1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesMaster", "rId5");
		checkRel(relationships, "/ppt/slides/slide3.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide", "rId8");
		checkRel(relationships, "/ppt/slides/slide2.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide", "rId7");
	}

	@Test
	public void testNormalizedRels() throws Exception {
		final Relationships relationships = new Relationships.Default(
			this.eventFactory,
			new RelationshipIdGeneration.Default(),
			new PartPath.Default("/ppt/slides")
		);
		try (final Reader reader = getReader("/slide1.xml.rels")) {
			relationships.readWith(this.inputFactory.createXMLEventReader(reader));
		}
		checkRel(relationships, "/ppt/notesSlides/notesSlide1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesSlide", "rId2");
		checkRel(relationships, "/ppt/slideLayouts/slideLayout1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout", "rId1");
	}

	@Test
	public void testByType() throws Exception {
		final PartPath basePath = new PartPath.Default("ppt/slides");
		final Relationships relationships = new Relationships.Default(
			this.eventFactory,
			new RelationshipIdGeneration.Default(),
			basePath
		);
		try (final Reader reader = getReader("/slide1.xml.rels")) {
			relationships.readWith(this.inputFactory.createXMLEventReader(reader));
		}
		List<Attribute> attributes = Arrays.asList(
			this.eventFactory.createAttribute(Relationship.ID, "rId2"),
			this.eventFactory.createAttribute(Relationship.TYPE, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesSlide"),
			this.eventFactory.createAttribute(Relationship.TARGET, "/ppt/notesSlides/notesSlide1.xml")
		);
		checkRelsByType(
			relationships,
			"http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesSlide",
			Collections.singletonList(
				new Relationship.Default(
					this.eventFactory,
					basePath,
					this.eventFactory.createStartElement(
						XMLConstants.DEFAULT_NS_PREFIX,
						XMLConstants.NULL_NS_URI,
						Relationship.NAME,
						attributes.iterator(),
						null
					)
				)
			)
		);
		attributes = Arrays.asList(
			this.eventFactory.createAttribute(Relationship.ID, "rId1"),
			this.eventFactory.createAttribute(Relationship.TYPE, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout"),
			this.eventFactory.createAttribute(Relationship.TARGET, "../slideLayouts/slideLayout1.xml")
		);
		checkRelsByType(
			relationships,
			"http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout",
			Collections.singletonList(
				new Relationship.Default(
					this.eventFactory,
					basePath,
					this.eventFactory.createStartElement(
						XMLConstants.DEFAULT_NS_PREFIX,
						XMLConstants.NULL_NS_URI,
						Relationship.NAME,
						attributes.iterator(),
						null
					)
				)
			)
		);
	}

	@Test
	public void testMissingRels() throws Exception {
		Document.General document = new Document.General(
			new ConditionalParameters(),
			inputFactory,
			outputFactory,
			eventFactory,
            new DispersedTranslations.Default(),
			"sd",
			root.in("/sampleMore.xlsx").asUri(),
			LocaleId.ENGLISH,
			StandardCharsets.UTF_8.name(),
			null,
			null,
			null
		);
		document.open();
		Relationships worksheetRels = document.relationshipsFor("xl/worksheets/sheet4.xml");
		assertNotNull(worksheetRels);
	}

	@Test
	public void testSkipInvalidRels() throws Exception {
		Document.General document = new Document.General(
			new ConditionalParameters(),
			inputFactory,
			outputFactory,
			eventFactory,
			new DispersedTranslations.Default(),
			"sd",
			root.in("/1046.pptx").asUri(),
			LocaleId.ENGLISH,
			OpenXMLFilter.ENCODING.name(),
			null,
			null,
			null
		);
		document.open();
		Relationships rootRels = document.relationshipsFor("");
		assertNotNull(rootRels);
	}

	private Reader getReader(String resource) {
		InputStream input = root.in(resource).asInputStream();
		return new InputStreamReader(input, OpenXMLFilter.ENCODING);
	}

	private void checkRel(Relationships rels, String target, String type, String id) {
		final Iterator<Relationship> ri = rels.with(id).iterator();
		assertTrue(ri.hasNext());
		final Relationship r = ri.next();
		assertEquals(id, r.id());
		assertEquals(type, r.type());
		assertEquals(target, r.target());
		assertEquals(Relationship.INTERNAL_TARGET_MODE, r.targetMode());
		assertFalse(ri.hasNext());
	}

	private void checkRelsByType(Relationships rels, String type, List<Relationship> expected) {
		final Iterator<Relationship> ri = rels.of(type).iterator();
		final Iterator<Relationship> eri = expected.iterator();
		while (ri.hasNext()) {
			assertTrue(eri.hasNext());
			final Relationship r = ri.next();
			final Relationship er = eri.next();
			assertEquals(er.id(), r.id());
			assertEquals(er.type(), r.type());
			assertEquals(er.target(), r.target());
			assertEquals(er.targetMode(), er.targetMode());
		}
		assertFalse(eri.hasNext());
	}
}
