/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@RunWith(JUnit4.class)
public class SerialDateTimeTest {
    @Test
    public void defaultBasedValuesTransformedToLocalDateTime() {
        final LocalDateTime baseDateTime = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);
        Assertions.assertThat(
            new SerialDateTime.Default("0", baseDateTime).asLocalDateTime().toString()
        ).isEqualTo("1970-01-01T00:00");
        Assertions.assertThat(
            new SerialDateTime.Default("1.101010101010101", baseDateTime).asLocalDateTime().toString()
        ).isEqualTo("1970-01-02T02:25:27.273");
        Assertions.assertThat(
            new SerialDateTime.Default("44463.042372685188", baseDateTime).asLocalDateTime().toString()
        ).isEqualTo("2091-09-26T01:01:01");
    }

    @Test
    public void epoch1900BasedValuesTransformedToLocalDateTime() {
        Assertions.assertThat(
            new SerialDateTime.Epoch1900Based("0").asLocalDateTime().toString()
        ).isEqualTo("1899-12-31T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1900Based("1").asLocalDateTime().toString()
        ).isEqualTo("1900-01-01T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1900Based("59").asLocalDateTime().toString()
        ).isEqualTo("1900-02-28T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1900Based("60").asLocalDateTime().toString()
        ).isEqualTo("1900-02-28T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1900Based("61").asLocalDateTime().toString()
        ).isEqualTo("1900-03-01T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1900Based("44463.042372685188").asLocalDateTime().toString()
        ).isEqualTo("2021-09-24T01:01:01");
    }

    @Test
    public void epoch1904BasedValuesTransformedToLocalDateTime() {
        Assertions.assertThat(
            new SerialDateTime.Epoch1904Based("0").asLocalDateTime().toString()
        ).isEqualTo("1904-01-01T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1904Based("1").asLocalDateTime().toString()
        ).isEqualTo("1904-01-02T00:00");
        Assertions.assertThat(
            new SerialDateTime.Epoch1904Based("44463.042372685188").asLocalDateTime().toString()
        ).isEqualTo("2025-09-25T01:01:01");
    }
}
