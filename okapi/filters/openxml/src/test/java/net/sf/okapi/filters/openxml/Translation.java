/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;

interface Translation {
    Event applyTo(final Event event);
    TextFragment applyTo(final TextFragment textFragment);

    final class Bypass implements Translation {
        @Override
        public Event applyTo(final Event event) {
            return event;
        }

        @Override
        public TextFragment applyTo(final TextFragment textFragment) {
            return textFragment;
        }
    }

    final class Copy extends GenericSkeletonWriter implements Translation {
        private final LocaleId targetLocale;

        Copy(final LocaleId targetLocale) {
            this.targetLocale = targetLocale;
        }

        @Override
        public Event applyTo(final Event event) {
            if (!event.isTextUnit()) {
                throw new IllegalArgumentException("The provided event type is unsupported: ".concat(event.toString()));
            }
            final ITextUnit tu = event.getTextUnit();
            final TextContainer tc = new TextContainer();
            tc.setContent(applyTo(tu.getSource().getFirstContent()));
            tu.setTarget(this.targetLocale, tc);
            return event;
        }

        @Override
        public TextFragment applyTo(final TextFragment textFragment) {
            return textFragment.clone();
        }
    }
}
