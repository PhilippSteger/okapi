/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.filters.cascadingfilter;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.filters.*;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.skeleton.ISkeletonWriter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * This filter is used to combine multiple filters.
 * It consists of a primary filter, followed by any number of sub-filters, all applied in sequence
 *
 * <p>
 * This filter can only be created via the provided factory method.
 * Also note {@link FilterParameters} do not implement {@link IParameters}. This class is best thought of
 * as a generator for a complex filter consisting of many {@link IPipelineStep}
 * This filter should not be added to a {@link IFilterConfigurationMapper} as it is not a true standalone
 * filter.
 * </p>
 */
public class CascadingFilter extends AbstractFilter {
    private IFilter primaryFilter;
    private List<SubFilterWrapperStep> subFiltersAsSteps;
    private List<Function<Stream<Event>, Stream<Event>>> stepsAsFunctions;
    private RawDocument input = null;
    private Queue<Event> eventQueue;

    // prevent instantiation of this class directly (use factory method)
    private CascadingFilter() {
    }

    public static CascadingFilter createCascadingFilter(FilterParameters parameters, IFilterConfigurationMapper mapper) throws OkapiException {
        CascadingFilter filter = new CascadingFilter();
        filter.primaryFilter = mapper.createFilter(parameters.getPrimaryConfigId());
        if (parameters.getPrimaryConfig() != null) {
            IParameters p = filter.primaryFilter.getParameters();
            p.fromString(parameters.getPrimaryConfig());
            filter.primaryFilter.setParameters(p);
        }
        filter.subFiltersAsSteps = createSubFilterAsSteps(parameters, filter.primaryFilter, mapper);
        filter.stepsAsFunctions = new ArrayList<>(filter.subFiltersAsSteps);
        filter.setMultilingual(filter.primaryFilter.isMultilingual());
        filter.setMimeType(filter.primaryFilter.getMimeType());
        filter.setName("okf_cascadingfilter");
        filter.setDisplayName("Cascading Filter");
        filter.setFilterWriter(filter.primaryFilter.createFilterWriter());
        return filter;
    }

    /**
     * Creates the subfilter steps needed by documents with multiple formats (JSON->HTML etc.).
     *
     * @param parameters   contains the subfilter custom configs (if any)
     * @param parentFilter the parent filter
     * @return list of subfilters as {@link SubFilterWrapperStep}
     */
    protected static List<SubFilterWrapperStep> createSubFilterAsSteps(FilterParameters parameters, IFilter parentFilter, IFilterConfigurationMapper mapper) {
        List<SubFilterWrapperStep> steps = new ArrayList<>();
        // create and configure each defined subfilter
        if (parameters.getSubFilterConfigIds() != null) {
            for (int i = 0; i < parameters.getSubFilterConfigIds().size(); i++) {
                String cid = parameters.getSubFilterConfigIds().get(i);
                IFilter sf = mapper.createFilter(cid);
                // set subfilter custom configs
                if (parameters.getSubFilterConfigs() != null) {
                    IParameters p = sf.getParameters();
                    p.fromString(parameters.getSubFilterConfigs().get(i));
                    sf.setParameters(p);
                }
                steps.add(new SubFilterWrapperStep(sf, parentFilter.getEncoderManager()));
            }
        }
        return steps;
    }

    @Override
    public void cancel() {
        primaryFilter.cancel();
    }

    @Override
    public void close() {
        if (input != null) {
            input.close();
        }
        primaryFilter.close();
        for (IPipelineStep s : subFiltersAsSteps) {
            s.close();
        }
        eventQueue.clear();
    }

    @Override
    public List<FilterConfiguration> getConfigurations() {
        List<FilterConfiguration> configurations = new ArrayList<>(primaryFilter.getConfigurations());
        for (SubFilterWrapperStep s : subFiltersAsSteps) {
            configurations.addAll(s.getSubFilter().getConfigurations());
        }
        return configurations;
    }

    @Override
    public void open(RawDocument input) {
        this.input = input;
        eventQueue = new LinkedList<>();
        for (IPipelineStep s : subFiltersAsSteps) {
            s.setSourceLocale(input.getSourceLocale());
            s.setTargetLocale(input.getTargetLocale());
            s.handleEvent(Event.createStartBatchEvent());
            s.handleEvent(Event.createStartBatchItemEvent());
        }
        primaryFilter.open(input);
    }

    /**
     * @return
     */
    @Override
    public ISkeletonWriter createSkeletonWriter() {
        return primaryFilter.createSkeletonWriter();
    }

    /**
     * @return
     */
    @Override
    public IFilterWriter createFilterWriter() {
        return primaryFilter.createFilterWriter();
    }

    @Override
    public boolean hasNext() {
        return primaryFilter.hasNext() || !eventQueue.isEmpty();
    }

    @Override
    public Event next() {
        // return events from queue first
        if (!eventQueue.isEmpty()) {
            return eventQueue.remove();
        }

        // get next primary filter event and send it down the pipeline of subfilters
        Event event = primaryFilter.next();
        if (event.isTextUnit()) {
            Stream.of(event).flatMap(e -> IPipelineStep.runPipelineSteps(stepsAsFunctions, Stream.of(e)))
                    .forEach(eventQueue::add);
            return eventQueue.remove();
        }
        return event;
    }

    @Override
    public Stream<Event> stream() {
        return primaryFilter.stream().flatMap(e -> IPipelineStep.runPipelineSteps(stepsAsFunctions, Stream.of(e)));
    }
}
