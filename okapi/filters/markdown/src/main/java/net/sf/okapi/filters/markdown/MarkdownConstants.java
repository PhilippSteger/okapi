package net.sf.okapi.filters.markdown;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MarkdownConstants {
	public static final Set<String> PAIRED_HTML_INLINE_CODES =
			new HashSet<>(Arrays.asList(
					"link", // <a>
					"abbr",
					"bold", // <b>
					"bdo",
					"big",
					"cite",
					"code",
					"del",
					"dfn",
					"em",
					"italic", // <i>
					"ins",
					"kbd",
					"label",
					"q",
					"ruby",
					"s",
					"samp",
					"small",
					"span",
					"strong",
					"sub",
					"sup",
					"textarea",
					"underlined",
					"tt",
					"var"
			));
}
